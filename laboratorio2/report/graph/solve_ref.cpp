//-----------------------------------------
// MC458 - 2s-2014
//
// Diego Luis de Souza	RA: 135491
// Ulisses Malta Santos	RA: 140958
//
// Maximal Independent Set (MIS) 
//-----------------------------------------

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <list>

#define foreach(x, v) for (typeof (v).begin() x=(v).begin(); x !=(v).end(); ++x)
#define D(x) cout << #x " is " << x << endl

using namespace std;
typedef unsigned int uint;

class Graph
{
   public:
      uint nvertex;
      uint nedges;
      vector< vector<int> > adjmat;
      list<int> vertices;
      Graph(uint nv, uint ne);
      int get_degree(int v);
      void remove_vertex(list<int>::iterator &v);
      void rebuild_graph(int *removed, int n);
      void remove_adj(int v, int *removed, int *num_rm);
      int find_cycles(uint *seq);
      void read_graph();
      void print_graph();
};

class MIS
{
   public:
      uint A1(Graph &G, uint *tam_ci, uint *seq_vert_ci);
      void A2(Graph &G, uint *tam_ci, uint *seq_vert_ci);
      void A3(Graph &G, uint *tam_ci, uint *seq_vert_ci);
      bool check_solution(Graph &G, uint *seq_vert_ci, uint size_ci);
   private:
      void copy(uint *a, uint *b, int n);
      void print_vector(vector<int>& v, string s);
};

int num_calls;
clock_t limit_a1;

//debug
void print_vector(vector<int>& v, string s, float time, int calls)
{
   sort(v.begin(), v.end());
   cout << s;
   printf(" %f %d ", time, calls);
   cout << ": [" << v.size() << "] ";
   for (int i = 0; i < v.size(); ++i)
      printf("%d ", v[i]);
   printf("\n");
}


int main(void)
{
   uint ng, ne, nv;
   scanf("%u %u %u", &ng, &nv, &ne);

   uint size_ci_1;
   uint size_ci_2;
   uint size_ci_3;
   uint seq1[nv];
   uint seq2[nv];
   uint seq3[nv];

   int num_calls1;
   int num_calls2;
   int num_calls3;

   clock_t clock_A1 = 0;
   clock_t clock_A2 = 0;
   clock_t clock_A3 = 0;
   uint a1;
   for (int i = 0; i < ng; ++i)
   {
      Graph G1(nv, ne);
      G1.read_graph();
      
      Graph G2 = G1;
      Graph G3 = G1;

      MIS M;
      clock_t number_clocks;

      num_calls = 0;
      limit_a1 = clock();
      number_clocks = clock();
      a1 = M.A1(G1, &size_ci_1, seq1);
      number_clocks = clock() - number_clocks;
      clock_A1 += number_clocks;
      num_calls1 = num_calls;

      num_calls = 0;
      number_clocks = clock();
      M.A2(G2, &size_ci_2, seq2);
      number_clocks = clock() - number_clocks;
      clock_A2 += number_clocks;
      num_calls2 = num_calls;

      num_calls = 0;
      number_clocks = clock();
      M.A3(G3, &size_ci_3, seq3);
      number_clocks = clock() - number_clocks;
      clock_A3 += number_clocks;
      num_calls3 = num_calls;

   }

   float time_A1 = (((float)clock_A1)/CLOCKS_PER_SEC)/ng;
   float time_A2 = (((float)clock_A2)/CLOCKS_PER_SEC)/ng;
   float time_A3 = (((float)clock_A3)/CLOCKS_PER_SEC)/ng;

   
   if(a1==1)
   {
      printf("%d %f %f %f %d %d %d\n", ne, time_A1, time_A2, time_A3, num_calls1, num_calls2, num_calls3);
   }
//   if(a1==1)
//   {
//   printf("%d %f %f %f %d %d %d\n", ne, time_A1, time_A2, time_A3, num_calls1, num_calls2, num_calls3);
//      vector<int> seq_sorted1(seq1, seq1+size_ci_1);
//      print_vector(seq_sorted1, "A1", time_A1, num_calls1);
//   }
//   else printf("A1 limit time\n");
   else
   {
      printf("%d %f %f %f %d %d %d\n", ne, 30.0, time_A2, time_A3, num_calls1, num_calls2, num_calls3);
   }

   //--------------debug ----------------------
   //vector<int> seq_sorted2(seq2, seq2+size_ci_2);
  // print_vector(seq_sorted2, "A2", time_A2, num_calls2);

   //--------------debug ----------------------
   //vector<int> seq_sorted3(seq3, seq3+size_ci_3);
   //print_vector(seq_sorted3, "A3", time_A3, num_calls3);
   //printf("\n");

   return 0;
}

Graph::Graph(uint nv, uint ne)
{
   nvertex = nv;
   nedges = ne;
   vector<int> foo(nv, 0);
   adjmat.resize(nv, foo);
   vertices.resize(nv, 0);
}

//debug
void Graph::print_graph()
{
   foreach(i, vertices)
   {
      printf("degree %d, vertex %d:", get_degree(*i), *i);
      for (int j = 0; j < nvertex; ++j)
      {
         if(adjmat[*i][j]==1)
            printf(" %d", j);
      }
      printf("\n");
   }
   printf("\n");
}

//le grafo
void Graph::read_graph()
{
   for (int j = 0; j < nedges; j++)
   {
      int v1, v2;
      scanf("%d %d", &v1, &v2);
      adjmat[v1][v2] = adjmat[v2][v1] = 1;
   }
   int k = 0;
   foreach(it, vertices)
   {
      *it = k++;
   }
}

//remove um unico vertice
inline void Graph::remove_vertex(list<int>::iterator &it)
{
   it = vertices.erase(it);
   nvertex--;
}

//insere vertices e reconstroi grafo atualizando o vetor degree
void Graph::rebuild_graph(int *removed, int n)
{
   list<int>::iterator t = vertices.begin();
   int i = 0;
   while (i<n && t!=vertices.end())
   {
      if(*t > removed[i])
      {
         vertices.insert(t, removed[i++]);
         nvertex++;
      }
      else t++;
   }
   while(i < n)
   {
      vertices.push_back(removed[i++]);
      nvertex++;
   }
}

//remove todos vertices adjacentes a v
void Graph::remove_adj(int v, int *removed, int *num_rm)
{
   list<int>::iterator it = vertices.begin();
   int d = get_degree(v);
   int offset = *num_rm;
   while(*num_rm < (offset+d) && it != vertices.end())
   {
      if(adjmat[v][*it]==1)
      {
         removed[*num_rm] = *it;
         *num_rm = *num_rm + 1;
         remove_vertex(it);
      }
      else it++;
   }
}

inline int Graph::get_degree(int v)
{
   int ans=0;
   foreach(it, vertices)
      if(adjmat[*it][v]==1)
         ans++;
   return ans;
}

//encontra todos os ciclos
int Graph::find_cycles(uint *seq)
{
   list<int> aux = vertices;
   list<int>::iterator icur = aux.begin();

   bool has_adjacent = true; //primeiro vertice escolhido sempre terá
   int cur=*icur, k=0, count=0;
   while(aux.size()>1)
   {
      if(!has_adjacent)
      {
         aux.erase(icur);
         icur = aux.begin();
         cur = *icur;
         count=0;
      }

      has_adjacent = false;
      list<int>::iterator it = aux.begin();
      while(it != aux.end())
      {
         if(adjmat[cur][*it]==1)
         {
            count++;
            has_adjacent = true;

            if(count%2) seq[k++]=cur;
            
            cur=*it;
            aux.erase(icur);
            icur = it;
            break;
         }
         it++;
      }
   }
   return k;
}

//debug, verifica se o conjunto eh independente (nao verifica se eh maximo)
bool MIS::check_solution(Graph &G, uint *seq_vert_ci, uint size_ci)
{
   int k = (size_ci*(size_ci-1))/2;
   for (int i = 0; i < k; ++i)
      for (int j = 0; j < size_ci; ++j)
         for (int t = j+1; t < size_ci; ++t)
            if(G.adjmat[seq_vert_ci[j]][seq_vert_ci[t]]) return false;
   return true;
}

inline void MIS::copy(uint *a, uint *b, int n)
{
   for (int i = 0; i < n; ++i) a[i]=b[i];
}

uint MIS::A1(Graph &G, uint *tam_ci, uint *seq_vert_ci)
{
   if(G.nvertex==0)
   {
      *tam_ci=0;
      return 1;
   }

   clock_t aux = clock()-limit_a1;
   if((((float)aux)/CLOCKS_PER_SEC)>30.0)
      return 0;  

   num_calls++;

   //salva iterador para o primeiro vertice
   list<int>::iterator it = G.vertices.begin();
   int v = *it;

   int removed[G.nvertex], num_rm=0;
   //G = G - v, remove primeiro vertice da lista de vertices disponiveis
   G.remove_vertex(it);
   removed[num_rm++] = v;

   if(A1(G, tam_ci, seq_vert_ci)==0)
      return 0;

   //G = G - v - Adj(V), remove todos os vertices adjacentes a v
   it = G.vertices.begin();
   while(it!=G.vertices.end())
   {
      if(G.adjmat[v][*it]==1)
      {
         removed[num_rm++] = *it;
         G.remove_vertex(it);
      }
      else it++;
   }
   
   uint size;
   uint seq[G.nvertex];
   if(A1(G, &size, seq)==0)
      return 0;
   
   size++;
   if(*tam_ci < size)
   {
      seq[size-1] = v;
      copy(seq_vert_ci, seq, size);
      *tam_ci = size;
   }
   //reconstroi grafo, insere v e seus vertices adjacentes novamente
   G.rebuild_graph(removed, num_rm);
   return 1;
}

void MIS::A2(Graph &G, uint *tam_ci, uint *seq_vert_ci)
{
   num_calls++;
   //caso base
   if(G.nvertex==0)
   {
      *tam_ci=0;
      return;
   }

   list<int>::iterator it;
   int removed[G.nvertex], num_rm=0;

   //Existe algum vertice v tal que d(v)<=1
   //∃ v | d(v) <= 1 ?
   it = G.vertices.begin();
   while(it != G.vertices.end())
   {
      if(G.get_degree(*it) <= 1) break;
      it++;
   }

   if(it!=G.vertices.end())
   {
      int v = *it;
      G.remove_vertex(it);
      removed[num_rm++] = v;
      if(G.get_degree(v)==1)
      { //d(v)=1
         G.remove_adj(v, removed, &num_rm);
      }

      A2(G, tam_ci, seq_vert_ci);
      seq_vert_ci[*tam_ci] = v;
      *tam_ci = *tam_ci+1;

      //reconstroi vertices disponiveis
      G.rebuild_graph(removed, num_rm);
      return;
   }

   //G é formado por apenas ciclos ?
   //se sim, podemos encontrar o MIS em tempo polinomial
   it = G.vertices.begin();
   while(it!=G.vertices.end())
   {
      if(G.get_degree(*it)!=2)
         break;
      it++;
   }
 
   if(it==G.vertices.end())
   {
      *tam_ci = G.find_cycles(seq_vert_ci);
      return;
   }

   //∃ v | d(v)>=3, e it tem esse valor

   //G0 = M - v
   int v = *it;
   G.remove_vertex(it);
   removed[num_rm++]=v;
   A2(G, tam_ci, seq_vert_ci);

   //G1 = M - v - Adj(V)
   G.remove_adj(v, removed, &num_rm);
      
   uint size;
   uint seq[G.nvertex];
   A2(G, &size, seq);
   
   size++;
   if(*tam_ci < size)
   {
      seq[size-1] = v;
      copy(seq_vert_ci, seq, size);
      *tam_ci = size;
   }
   G.rebuild_graph(removed, num_rm);
}

void MIS::A3(Graph &G, uint *tam_ci, uint *seq_vert_ci)
{
   num_calls++;
   //caso base
   if(G.nvertex==0)
   {
      *tam_ci=0;
      return;
   }

   list<int>::iterator it;
   int removed[G.nvertex], num_rm=0;

   //Existe algum vertice v tal que d(v)<=1
   //∃ v | d(v) <= 1 ?
   
   *tam_ci = 0;
   it = G.vertices.begin();
   while(it != G.vertices.end())
   {
      int d = G.get_degree(*it);
      if(d <= 1)
      {
         seq_vert_ci[(*tam_ci)++] = *it;
         list<int>::iterator i = G.vertices.begin();
         if(d==1)
         {
            while(G.adjmat[*it][*i]==0) i++;
            removed[num_rm++] = *i;
            G.vertices.erase(i);
            G.nvertex--;
         }
         removed[num_rm++] = *it;
         it = G.vertices.erase(it);
         G.nvertex--;
      }
      else it++;
   }

  if(G.vertices.empty())
  {
     G.rebuild_graph(removed, num_rm);
     return;
  }

   //G é formado por apenas ciclos ?
   //se sim, podemos encontrar o MIS em tempo polinomial
   it = G.vertices.begin();
   while(it!=G.vertices.end())
   {
      if(G.get_degree(*it)!=2)
         break;
      it++;
   }
 
   if(it==G.vertices.end())
   {
      int mis_cycle;
      uint vert_cycle[G.nvertex];
      mis_cycle = G.find_cycles(vert_cycle);
      for(int i=*tam_ci; i<*tam_ci+mis_cycle; i++)
         seq_vert_ci[i] = vert_cycle[i-*tam_ci];
      *tam_ci += mis_cycle;
     G.rebuild_graph(removed, num_rm);
     return;
   }

   //∃ v | d(v)>=3, e it tem esse valor

   //G0 = M - v, d(v) = min(d(vi))
   list<int>::iterator t = G.vertices.begin();
   int a = G.get_degree(*t);
   t++;
   while(t!=G.vertices.end())
   {
      int d = G.get_degree(*t);
      if(a < d)
      {
         a = d;
         it = t;
      }
      t++;
   }

   int v = *it;
   G.remove_vertex(it);
   removed[num_rm++]=v;

   uint size;
   uint seq[G.nvertex];

   A3(G, &size, seq);

   //G1 = M - v - Adj(V)
   G.remove_adj(v, removed, &num_rm);
   int save_tam = *tam_ci;
   A3(G, tam_ci, seq_vert_ci + save_tam);
   
   (*tam_ci)++;
   if(*tam_ci > size)
   {
      *tam_ci += save_tam;
      seq_vert_ci[*tam_ci-1] = v;
   }
   else
   {
      copy(seq_vert_ci+save_tam, seq, size);
      *tam_ci = size + save_tam;
   }
   G.rebuild_graph(removed, num_rm);
}

