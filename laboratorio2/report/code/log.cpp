class Log
{
   public:
      uint num_graphs;
      uint num_vertices;
      uint num_edges;
      const char *input_file;
      uint tam;
      float m_time;
      const char *method;
      vector<int> v;
      uint num_calls;      
      uint otimo;     

      Log(uint ng, uint nv, uint ne, const char *in_file);
      void print_log();
};