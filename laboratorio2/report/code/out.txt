<RA> <br> A1 <br> <Arquivo> <br> 1 <br> <NV> <br> <NE> <br> <ótimo> <br> <NChamadas> <br> <Tam_CI> <br> <Seq_Vert_CI>

- <RA> É da forma raXXXXXX_YYYYYY, com XXXXXX e YYYYYY os RA's dos alunos;
- <br> representa um espaço em branco;
- A1, A2 ou A3 é o nome do algoritmo usado para os resultados da linha correspondente;
- <Arquivo> é o nome do arquivo lido de onde os testes da linha foram realizados;
- 1, 2, 3,... é o número do grafo dentro do arquivo (o primeiro grafo do arquivo tem número 1);
- <NV> é o número de vértices do correspondente grafo;
- <NE> é o número de arestas do correspondente grafo;
- <ótimo> é igual a 1 (se terminou dentro do tempo máximo e obteve solução ótima) ou 0 (se parou por ter atingido o limite de tempo máximo e não sabe se a solução é de fato uma solução ótima;
- <NChamadas> é o número de chamadas recursivas do algoritmo em questão (a primeira chamada já conta 1);
- <Tam_CI> é a cardinalidade do conjunto independente encontrado para o correspondente grafo/algoritmo;
- <Seq_Vert_CI> é a sequência de vértices do conjunto independente encontrado, separados por espaço em branco;
