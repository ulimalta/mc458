class MIS
{
   public:
      uint A1(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo);
      uint A2(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo);
      uint A3(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo);
      uint check_solution(Graph &G, uint *seq_vert_ci, uint size_ci);

   private:
      bool check_time(uint tempo_maximo);
      void copy(uint *a, uint *b, int n);
};

