int main(int argc, char *argv[])
{
   if(argc != 3)
   {
      printf("Argumento incorreto!!! <arquivo de dados> <tempo maximo>\n");
      return 0;
   }

   FILE *data_file = fopen(argv[1], "r");

   if(data_file == NULL)
   {
      printf("Erro ao abrir o arquivo de dados!!!\n");
      return 0;
   }

   uint tempo_maximo = atoi(argv[2]);
   uint ng, ne, nv;

   fscanf(data_file, "%u %u %u", &ng, &nv, &ne);

   for(int i = 0; i < ng; ++i)
   {
      Graph G1(nv, ne);
      G1.read_graph(data_file);

      Graph G2 = G1;
      Graph G3 = G1;

      MIS M;
      Log L(i+1, nv, ne, argv[1]);

      uint size_ci, seq[nv], rec_calls;

      //Algoritmo A1
      rec_calls = 1;
      number_clocks = clock();
      L.otimo = M.A1(G1, &size_ci, seq, &rec_calls, tempo_maximo);
      number_clocks = clock() - number_clocks;
      vector<int> seq_sorted1(seq, seq+size_ci);

      //Log
      L.tam = size_ci;
      L.m_time = ((float)number_clocks)/CLOCKS_PER_SEC;
      L.method = "A1";
      L.v = seq_sorted1;
      L.num_calls = rec_calls;
      L.print_log();

      //Algoritmo A2
      rec_calls = 1;
      number_clocks = clock();
      L.otimo = M.A2(G2, &size_ci, seq, &rec_calls, tempo_maximo);
      number_clocks = clock() - number_clocks;
      vector<int> seq_sorted2(seq, seq+size_ci);

      //Log
      L.tam = size_ci;
      L.m_time = ((float)number_clocks)/CLOCKS_PER_SEC;
      L.method = "A2";
      L.v = seq_sorted2;
      L.num_calls = rec_calls;
      L.print_log();      

      //Algoritmo A3
      rec_calls = 1;
      number_clocks = clock();
      L.otimo = M.A3(G3, &size_ci, seq, &rec_calls, tempo_maximo);
      number_clocks = clock() - number_clocks;
      vector<int> seq_sorted3(seq, seq+size_ci);

      //Log
      L.tam = size_ci;
      L.m_time = ((float)number_clocks)/CLOCKS_PER_SEC;
      L.method = "A3";
      L.v = seq_sorted3;
      L.num_calls = rec_calls;
      L.print_log();

      FILE *output_file = fopen("ra135491_140958.log", "a");
      fprintf(output_file, "\n");
      fclose(output_file);
   }

   fclose(data_file);

   return 0;
}

