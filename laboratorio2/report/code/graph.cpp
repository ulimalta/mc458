class Graph
{
   public:
      uint nvertex;
      uint nedges;
      vector< vector<int> > adjmat;
      list<int> vertices;

      Graph(uint nv, uint ne);
      int get_degree(int v);
      void remove_vertex(list<int>::iterator &v);
      void rebuild_graph(int *removed, int n);
      void remove_adj(int v, int *removed, int *num_rm);
      int find_cycles(uint *seq);
      void read_graph(FILE *data_file);
};
