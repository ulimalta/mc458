#include <bits/stdc++.h>

#define foreach(x, v) for (typeof (v).begin() x=(v).begin(); x !=(v).end(); ++x)
#define For(i, a, b) for (int i=(a); i<(b); i++)
#define D(x) cout << #x " is " << x << endl

#define MAX 100
using namespace std;
typedef unsigned int uint;


class Graph
{
   public:
      uint nvertex;
      uint nedges;
      int adjmat[MAX][MAX];
      list<int> vertices;
      int degree[MAX];
      Graph(uint nv, uint ne);
      void read_graph();
      void print_graph();
};

class MIS
{
   public:
      void A1(Graph *G, uint *tam_ci, uint *seq_vert_ci);
      void A2(Graph *G, uint *tam_ci, uint *seq_vert_ci);
   private:
      void copy(uint *a, uint *b, int n);
};

int main(void)
{
   uint ng, ne, nv;
   scanf("%u %u %u", &ng, &nv, &ne);
   for (int i = 0; i < ng; ++i)
   {
      Graph G(nv, ne);
      G.read_graph();
      //G.print_graph();

      uint size_ci;
      uint seq[nv];

      MIS M;
      M.A1(&G, &size_ci, seq);
      printf("Maximal independent set %d\n", size_ci);
      for (int i = 0; i < size_ci; ++i)
         printf("%d ", seq[i]);
      printf("\n");
   }
   return 0;
}

//debug
void Graph::print_graph()
{
   foreach(i, vertices)
   {
      printf("degree %d, vertex %d:", degree[*i], *i);
      for (int j = 0; j < nvertex; ++j)
      {
         if(adjmat[*i][j])
            printf(" %d", j);
      }
      printf("\n");
   }
   printf("\n");
}

Graph::Graph(uint nv, uint ne)
{
   nvertex = nv;
   nedges = ne;
   for (int i = 0; i < nv; ++i)
   {
      for (int j = 0; j < nv; ++j)
      {
         adjmat[i][j]=0;
      }
      degree[i]=0;
   }
   vertices.resize(nv, 0);
}

void Graph::read_graph()
{
   for (int j = 0; j < nedges; j++)
   {
      int v1, v2;
      scanf("%d %d", &v1, &v2);
      adjmat[v1][v2] = adjmat[v2][v1] = 1;
      degree[v1]++;
      degree[v2]++;
   }
   int k = 0;
   foreach(it, vertices)
      *it = k++;
}

inline void MIS::copy(uint *a, uint *b, int n)
{
   for (int i = 0; i < n; ++i) a[i]=b[i];
}

void MIS::A1(Graph *G, uint *tam_ci, uint *seq_vert_ci)
{
   if(!G->nvertex)
   {
      *tam_ci=0;
      return;
   }

   int v = (G->vertices).back();

   //G0 = M - v
   Graph G0 = *G;
   (G0.vertices).pop_back();
   (G0.nvertex)--;

   //G1 = M - v - Adj(V)
   Graph G1 = *G;
   (G1.vertices).pop_back();
   (G1.nvertex)--;
   foreach(j, G->vertices)
   {
      if(G->adjmat[v][*j]==1)
      {
         (G1.vertices).remove(*j);
         (G1.nvertex)--;
      }
   }
      
   uint seq0[G->nvertex];
   uint seq1[G->nvertex];

   uint size0, size1;
   A1(&G0, &size0, seq0);
   A1(&G1, &size1, seq1);
   
   size1++;
   if(size0 > size1)
   {
      copy(seq_vert_ci, seq0, size0);
      *tam_ci = size0;
   }
   else
   {
      seq1[size1-1] = v;
      copy(seq_vert_ci, seq1, size1);
      *tam_ci = size1;
   }
}

void MIS::A2(Graph *G, uint *tam_ci, uint *seq_vert_ci)
{
   if(!G->nvertex)
   {
      *tam_ci=0;
      return;
   }

   foreach(i, G->vertices)
      if(degree[*i] == 1)
      {
         foreach(j, G->vertices)
            if(G->adjmat[*i][*j]==1)
            {
               
            }
      }

   int v = (G->vertices).back();

   //G0 = M - v
   Graph G0 = *G;
   (G0.vertices).pop_back();
   (G0.nvertex)--;

   //G1 = M - v - Adj(V)
   Graph G1 = *G;
   (G1.vertices).pop_back();
   (G1.nvertex)--;
   foreach(j, G->vertices)
   {
      if(G->adjmat[v][*j]==1)
      {
         (G1.vertices).remove(*j);
         (G1.nvertex)--;
      }
   }

   uint seq0[G->nvertex];
   uint seq1[G->nvertex];

   uint size0, size1;
   A1(&G0, &size0, seq0);
   A1(&G1, &size1, seq1);

   size1++;
   if(size0 > size1)
   {
      copy(seq_vert_ci, seq0, size0);
      *tam_ci = size0;
   }
   else
   {
      seq1[size1-1] = v;
      copy(seq_vert_ci, seq1, size1);
      *tam_ci = size1;
   }
}
