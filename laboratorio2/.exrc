if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
imap <S-Tab> <Plug>SuperTabBackward
inoremap <silent> <C-Tab> =UltiSnips#ListSnippets()
inoremap <C-S-Tab> :tabpreviousi
map! <S-Insert> <MiddleMouse>
snoremap <silent>  c
xnoremap <silent> 	 :call UltiSnips#SaveLastVisualSelection()gvs
snoremap <silent> 	 :call UltiSnips#ExpandSnippet()
nmap  :up
nnoremap  :tabnew
nnoremap  :q
nnoremap <silent> \p :call conque_gdb#print_word(expand("<cword>"))
nnoremap <silent> \t :call conque_gdb#command("backtrace")
nnoremap <silent> \f :call conque_gdb#command("finish")
nnoremap <silent> \s :call conque_gdb#command("step")
nnoremap <silent> \n :call conque_gdb#command("next")
nnoremap <silent> \r :call conque_gdb#command("run")
nnoremap <silent> \c :call conque_gdb#command("continue")
nnoremap <silent> \b :call conque_gdb#toggle_breakpoint(expand("%:p"), line("."))
nmap gx <Plug>NetrwBrowseX
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#NetrwBrowseX(expand("<cWORD>"),0)
snoremap <silent> <Del> c
snoremap <silent> <BS> c
snoremap <silent> <C-Tab> :call UltiSnips#ListSnippets()
nnoremap <silent> <F11> :call conque_term#exec_file()
nmap <silent> <M-Right> :wincmd l
nmap <silent> <M-Left> :wincmd h
nmap <silent> <M-Down> :wincmd j
nmap <silent> <M-Up> :wincmd k
nnoremap <C-Tab> :tabnext
nnoremap <C-S-Tab> :tabprevious
map <S-Insert> <MiddleMouse>
inoremap <silent> 	 =UltiSnips#ExpandSnippet()
imap  :up
imap  ==a
inoremap  :q
inoremap AA A
inoremap CC C
inoremap DD dd
inoremap II I
inoremap OO O
inoremap SS S
inoremap UU u
inoremap { {}O
let &cpo=s:cpo_save
unlet s:cpo_save
set autochdir
set background=dark
set backspace=indent,eol,start
set backup
set backupdir=~/.vim/backups//
set cindent
set directory=~/.vim/swapfiles//
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set guifont=Monospace\ 11
set guioptions=aegimt
set helplang=en
set hidden
set ignorecase
set incsearch
set laststatus=2
set mouse=a
set printoptions=paper:a4
set ruler
set runtimepath=~/.vim,~/.vim/bundle/Vundle.vim,~/.vim/bundle/supertab,~/.vim/bundle/vim-airline,~/.vim/bundle/ultisnips,~/.vim/bundle/vim-snippets,/var/lib/vim/addons,/usr/share/vim/vimfiles,/usr/share/vim/vim74,/usr/share/vim/vimfiles/after,/var/lib/vim/addons/after,~/.vim/after,~/.vim/bundle/Vundle.vim,~/.vim/bundle/Vundle.vim/after,~/.vim/bundle/supertab/after,~/.vim/bundle/vim-airline/after,~/.vim/bundle/ultisnips/after,~/.vim/bundle/vim-snippets/after
set shiftwidth=3
set smartcase
set smartindent
set statusline=\ %{HasPaste()}%<%-15.25(%f%)%m%r%h\ %w\ \ \ \ \ [%{&ff}/%Y]\ \ \ %<%20.30(%{hostname()}:%{CurDir()}%)\ %=%-10.(%l,%c%V%)\ %p%%/%L
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set tabstop=3
set termencoding=utf-8
set window=49
" vim: set ft=vim :
