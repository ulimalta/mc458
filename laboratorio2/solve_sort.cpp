#include <bits/stdc++.h>

#define foreach(x, v) for (typeof (v).begin() x=(v).begin(); x !=(v).end(); ++x)
#define For(i, a, b) for (int i=(a); i<(b); i++)
#define D(x) cout << #x " is " << x << endl

#define id first
#define deg second
#define MAX 100
using namespace std;
typedef unsigned int uint;
typedef pair<int, int> ii;

//debug
int num_calls=0;

class Graph
{
   public:
      uint nvertex;
      uint nedges;
      int adjmat[MAX][MAX];
      list< ii > vertices;
  //    int degree[MAX];
      Graph(uint nv, uint ne);
      void remove_vertex(int v);
      void read_graph();
      void print_graph();
      int find_cycles(uint *seq);
//      void sort_bydegree();
};

class MIS
{
   public:
      void A1(Graph *G, uint *tam_ci, uint *seq_vert_ci);
      void A2(Graph *G, uint *tam_ci, uint *seq_vert_ci);
      bool check_solution(Graph *G, uint *seq_vert_ci, uint size_ci);
   private:
      void copy(uint *a, uint *b, int n);
};

int main(void)
{
   uint ng, ne, nv;
   scanf("%u %u %u", &ng, &nv, &ne);

   clock_t clock_A1 = 0;
   clock_t clock_A2 = 0;
   for (int i = 0; i < ng; ++i)
   {
      Graph G(nv, ne);
      G.read_graph();
      //G.print_graph();

      uint size_ci;
      uint seq[101];

      clock_t number_clocks;
      MIS M;
//      number_clocks = clock();
//      M.A1(&G, &size_ci, seq);
//      number_clocks = clock() - number_clocks;
//      clock_A1 += number_clocks;
////
//      //--------------debug ----------------------
//      vector<int> seq_sorted0(seq, seq+size_ci);
//      sort(seq_sorted0.begin(), seq_sorted0.end());
//      printf("--------------\n");
//      printf("A1: Maximal independent set %d\n", size_ci);
//      for (int i = 0; i < size_ci; ++i)
//         printf("%d ", seq_sorted0[i]);
//      printf("\n");
//      printf("--------------\n");
//      
      //--------------debug ----------------------

      number_clocks = clock();
      M.A2(&G, &size_ci, seq);
      number_clocks = clock() - number_clocks;
      clock_A2 += number_clocks;

      //--------------debug ----------------------
     vector<int> seq_sorted1(seq, seq+size_ci);
     sort(seq_sorted1.begin(), seq_sorted1.end());
     printf("--------------\n");
     printf("A2: Maximal independent set %d\n", size_ci);
     for (int i = 0; i < size_ci; ++i)
        printf("%d ", seq_sorted1[i]);
     printf("\n");
     printf("--------------\n");

//      if(M.check_solution(&G, seq, size_ci))
//         printf("Correct\n");
//      else
//         printf("Think More\n");

      //--------------debug ----------------------
   }
   float time_A1 = (((float)clock_A1)/CLOCKS_PER_SEC)/ng;
   float time_A2 = (((float)clock_A2)/CLOCKS_PER_SEC)/ng;
   printf("Time A1: %f\n", time_A1);
   printf("Time A2: %f\n", time_A2);
   return 0;
}

Graph::Graph(uint nv, uint ne)
{
   nvertex = nv;
   nedges = ne;
   for (int i = 0; i < nv; ++i)
      for (int j = 0; j < nv; ++j)
         adjmat[i][j]=0;
   vertices.resize(nv, make_pair(0,0));
}

void Graph::remove_vertex(int v)
{
   foreach(it, vertices)
   {
      if(adjmat[it->id][v]==1)
      {
         adjmat[it->id][v]=0;
         (it->deg)--;
      }
   }
   foreach(it, vertices)
      if(it->id==v)
      {
         it=vertices.erase(it);
         break;
      }
   nvertex--;
}

//debug
void Graph::print_graph()
{
   foreach(i, vertices)
   {
      printf("degree %d, vertex %d:", i->deg, i->id);
      for (int j = 0; j < nvertex; ++j)
      {
         if(adjmat[i->id][j])
            printf(" %d", j);
      }
      printf("\n");
   }
   printf("\n");
}

void Graph::read_graph()
{
   int degree[nvertex];
   memset(degree, 0, sizeof(degree));
   for (int j = 0; j < nedges; j++)
   {
      int v1, v2;
      scanf("%d %d", &v1, &v2);
      adjmat[v1][v2] = adjmat[v2][v1] = 1;
      degree[v1]++;
      degree[v2]++;
   }
   int k = 0;
   foreach(it, vertices)
   {
      it->id = k++;
      it->deg = degree[it->id];
   }
}

int Graph::find_cycles(uint *seq)
{
   list<ii> aux = vertices;
   list<ii>::iterator icur = aux.begin();

   bool has_adjacent = true; //primeiro vertice escolhido sempre terá
   int cur=icur->id, k=0, count=0;
   while(aux.size()>1)
   {
      if(!has_adjacent)
      {
         aux.erase(icur);
         icur = aux.begin();
         cur = icur->id;
         count=0;
      }

      has_adjacent = false;
      list<ii>::iterator it = aux.begin();
      while(it != aux.end())
      {
         if(adjmat[cur][it->id]==1)
         {
            count++;
            has_adjacent = true;

            if(count%2) seq[k++]=cur;
            
            cur=it->id;
            aux.erase(icur);
            icur = it;
            break;
         }
         it++;
      }
   }
   return k;
}

//debug
bool MIS::check_solution(Graph *G, uint *seq_vert_ci, uint size_ci)
{
   int k = (size_ci*(size_ci-1))/2;
   for (int i = 0; i < k; ++i)
      for (int j = 0; j < size_ci; ++j)
         for (int t = j+1; t < size_ci; ++t)
            if(G->adjmat[seq_vert_ci[j]][seq_vert_ci[t]]==1) return false;
   return true;
}

inline void MIS::copy(uint *a, uint *b, int n)
{
   for (int i = 0; i < n; ++i) a[i]=b[i];
}

void MIS::A1(Graph *G, uint *tam_ci, uint *seq_vert_ci)
{
   if(!G->nvertex)
   {
      *tam_ci=0;
      return;
   }

   ii v = (G->vertices).back();

   //G0 = M - v
   Graph G0 = *G;
   G0.remove_vertex(v.id);

   //G1 = M - v - Adj(V)
   Graph G1 = *G;
   G1.remove_vertex(v.id);
   foreach(j, G->vertices)
   {
      if(G1.adjmat[v.id][j->id]==1)
         G1.remove_vertex(j->id);
   }
      
   uint seq0[G->nvertex];
   uint seq1[G->nvertex];

   uint size0, size1;
   A1(&G0, &size0, seq0);
   A1(&G1, &size1, seq1);
   
   size1++;
   if(size0 > size1)
   {
      copy(seq_vert_ci, seq0, size0);
      *tam_ci = size0;
   }
   else
   {
      seq1[size1-1] = v.id;
      copy(seq_vert_ci, seq1, size1);
      *tam_ci = size1;
   }

}

void MIS::A2(Graph *G, uint *tam_ci, uint *seq_vert_ci)
{
   if(!G->nvertex)
   {
      *tam_ci=0;
      return;
   }

   ii v = (G->vertices).front();

   //G is Cn ?
   int count = 0;
   foreach(it, G->vertices)
   {
      if(it->second!=2) break;
      count++;
   }

   if(count==G->nvertex)
   {
      *tam_ci = G->find_cycles(seq_vert_ci);
      return;
   }
   //if G=Cn, we can solve it in polynomial time
//   if(count==G->nvertex)
//   {
//      //find cycle
//      int bf=v.id, cur=v.id, k=0, count=0;
//      do{
//      foreach(i, G->vertices)
//         if(i->id!=bf && G->adjmat[cur][i->id]==1)
//         {
//            count++;
//            if(count%2)
//               seq_vert_ci[k++]=cur;
//            bf=cur;
//            cur=i->id;
//            break;
//         }
//      }while(cur!=v.id);
//      *tam_ci = G->nvertex/2;
//      return;
//   }


   //∃ v | d(v) <= 1 ?
   count = 0;
   foreach(it, G->vertices)
   {
      if(it->deg <= 1)
      {
         v = *it;
         break;
      }
      count++;
   }

   //d(v) <= 1
   if(count < G->nvertex)
   {
      uint seq0[G->nvertex];
      uint size0;
      Graph G0 = *G;

      //since "vertices" is sorted by degree, front() has the minimum degree
      G0.remove_vertex(v.id);

      if(v.deg==1) //d(v)=1
      {
         foreach(j, G->vertices)
            if(G0.adjmat[v.id][j->id]==1)
            {
               G0.remove_vertex(j->id);
               break;
            }
      }
      A2(&G0, &size0, seq0);
      copy(seq_vert_ci, seq0, size0);
      seq_vert_ci[size0] = v.id;
      *tam_ci = size0+1;
      return;
   }

   //∃ v | d(v)>=3, find it
   foreach(it, G->vertices)
      if(it->deg >= 3)
      {
         v = *it;
         break;
      }

   //G0 = M - v
   Graph G0 = *G;
   G0.remove_vertex(v.id);

   //G1 = M - v - Adj(V)
   Graph G1 = *G;
   G1.remove_vertex(v.id);
   foreach(j, G->vertices)
   {
      if(G1.adjmat[v.id][j->id]==1)
         G1.remove_vertex(j->id);
   }
      
   uint seq0[G->nvertex];
   uint seq1[G->nvertex];

   uint size0, size1;
   A2(&G0, &size0, seq0);
   A2(&G1, &size1, seq1);
   
   size1++;
   if(size0 > size1)
   {
      copy(seq_vert_ci, seq0, size0);
      *tam_ci = size0;
   }
   else
   {
      seq1[size1-1] = v.id;
      copy(seq_vert_ci, seq1, size1);
      *tam_ci = size1;
   }
}

