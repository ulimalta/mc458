//-----------------------------------------
// MC458 - 2s-2014
//
// Diego Luis de Souza  RA: 135491
// Ulisses Malta Santos RA: 140958
//
// Maximal Independent Set (MIS)
//-----------------------------------------

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <list>
#include <ctime>
#include <cstring>

//Macro para foreach
#define foreach(x, v) for (typeof (v).begin() x = (v).begin(); x != (v).end(); ++x)
#define D(x) cout << #x " is " << x << endl

using namespace std;

typedef unsigned int uint;

//Estrutura de dados utilizada para representar o grafo
class Graph
{
   public:
      uint nvertex;                     // numero de vertices
      uint nedges;                      // numero de arestas
      vector< vector<int> > adjmat;     // matrix de adjacencia
      list<int> vertices;               // lista que contem os vertices disponiveis

      //construtor
      Graph(uint nv, uint ne);

      //obtem o grau do vertice 'v'
      int get_degree(int v);         

      //remove um vertice da lista pelo iterador   
      void remove_vertex(list<int>::iterator &v); 

      //reconstroi a lista de vertices dispoiveis, acrescentando todos os vertices do vertor removed
      void rebuild_graph(int *removed, int n);            

      //remove todos todos os vertices adjacentes ao vertice 'v' acrescentando ao vetor removed e atualizando o tamanho do vetor 'num_rm'
      void remove_adj(int v, int *removed, int *num_rm);   

      //caso o grafo seja formado por apenas ciclos, encontra todos os ciclos a armazena no vetor seq
      int find_cycles(uint *seq);

      //le o grafo do arquivo do qual passamos o ponteiro
      void read_graph(FILE *data_file);
};

//Estrutura que represeta os algoritmos para solucao do problema de
//encontrar o Conjunto Independente Maximo (MIS)
class MIS
{
   public:
      //G (Grafo)
      //tam_ci (apontador para o tamanho do MIS)
      //seq_vert_ci (vertices do MIS)
      //rec_calls (apontador para o numero de chamadas recursivas)
      //tempo_maximo (tempo maximo que o algoritmo pode executar)
      //retorna 1 se executou dentro do tempo especificado, 0 caso contrario
      uint A1(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo);
      uint A2(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo);
      uint A3(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo);

      //verifica se o conjunto de vertices passado no vetor seq_vert_ci eh independete (nao verifica se em maximal)
      uint check_solution(Graph &G, uint *seq_vert_ci, uint size_ci);
   private:
      //verifica se ainda esta dentro do tempo especificado 
      bool check_time(uint tempo_maximo);

      //faz uma copia simples do vetor b para o vetor a
      void copy(uint *a, uint *b, int n);
};

//Estrutura utilizada para gerar o arquivo de Log
class Log
{
   public:
      uint num_graphs;            //numero de grafos
      uint num_vertices;          //numero de vertices
      uint num_edges;             //numero de arestas
      const char *input_file;     //nome do arquivo de entrada
      uint tam;                   //tamanho do MIS
      float m_time;               //tempo que o algoritmo levou para executar
      const char *method;         //nome do algoritmo (A1, A2 ou A3)
      vector<int> v;             //vetor com os vertices do MIS
      uint num_calls;            //numero de chamdas recursivas
      uint otimo;                //otimo=1 se executar dentro do tempo, otimo=0 caso contrario

      //construtor
      Log(uint ng, uint nv, uint ne, const char *in_file);
      //escreve dados no arquivo de log
      void print_log();
};

clock_t number_clocks;

int main(int argc, char *argv[])
{
   if(argc != 3)
   {
      printf("Argumento incorreto!!! <arquivo de dados> <tempo maximo>\n");
      return 0;
   }

   FILE *data_file = fopen(argv[1], "r");

   if(data_file == NULL)
   {
      printf("Erro ao abrir o arquivo de dados!!!\n");
      return 0;
   }

   uint tempo_maximo = atoi(argv[2]);
   uint ng, ne, nv;

   fscanf(data_file, "%u %u %u", &ng, &nv, &ne);

   for(int i = 0; i < ng; ++i)
   {
      Graph G1(nv, ne);
      G1.read_graph(data_file);

      Graph G2 = G1;
      Graph G3 = G1;

      MIS M;
      Log L(i+1, nv, ne, argv[1]);

      uint size_ci, seq[nv], rec_calls;

      //Algoritmo A1
      rec_calls = 1;
      number_clocks = clock();
      L.otimo = M.A1(G1, &size_ci, seq, &rec_calls, tempo_maximo);
      number_clocks = clock() - number_clocks;
      vector<int> seq_sorted1(seq, seq+size_ci);

      //Log
      L.tam = size_ci;
      L.m_time = ((float)number_clocks)/CLOCKS_PER_SEC;
      L.method = "A1";
      L.v = seq_sorted1;
      L.num_calls = rec_calls;
      L.print_log();

      //Algoritmo A2
      rec_calls = 1;
      number_clocks = clock();
      L.otimo = M.A2(G2, &size_ci, seq, &rec_calls, tempo_maximo);
      number_clocks = clock() - number_clocks;
      vector<int> seq_sorted2(seq, seq+size_ci);

      //Log
      L.tam = size_ci;
      L.m_time = ((float)number_clocks)/CLOCKS_PER_SEC;
      L.method = "A2";
      L.v = seq_sorted2;
      L.num_calls = rec_calls;
      L.print_log();      

      //Algoritmo A3
      rec_calls = 1;
      number_clocks = clock();
      L.otimo = M.A3(G3, &size_ci, seq, &rec_calls, tempo_maximo);
      number_clocks = clock() - number_clocks;
      vector<int> seq_sorted3(seq, seq+size_ci);

      //Log
      L.tam = size_ci;
      L.m_time = ((float)number_clocks)/CLOCKS_PER_SEC;
      L.method = "A3";
      L.v = seq_sorted3;
      L.num_calls = rec_calls;
      L.print_log();

      FILE *output_file = fopen("ra135491_140958.log", "a");
      fprintf(output_file, "\n");
      fclose(output_file);
   }

   fclose(data_file);

   return 0;
}

//Construtor Log
Log::Log(uint ng, uint nv, uint ne, const char *in_file)
{
   num_graphs = ng;
   num_vertices = nv;
   num_edges = ne;
   input_file = in_file;
}

//Escreve em um arquivo o Log do algoritmo correspondente
void Log::print_log()
{
   FILE *output_file;
   output_file = fopen("ra135491_140958.log", "a");
   if(output_file == NULL)
      return;
   fprintf(output_file, "ra135491_140958 %s %s %u %u %u %u ", method, input_file, num_graphs, num_vertices, num_edges, otimo);
   if(otimo == 1)
   {
      fprintf(output_file, "%u %u ", num_calls, tam);
      sort(v.begin(), v.end());
      for(int i = 0; i < v.size(); i++)
         fprintf(output_file, "%d ", v[i]);
   }
   fprintf(output_file, "\n");
   fclose(output_file);
}

//Construtor grafo
Graph::Graph(uint nv, uint ne)
{
   nvertex = nv;
   nedges = ne;
   vector<int> foo(nv, 0);
   //inicia matriz de adjacencia
   adjmat.resize(nv, foo);
   //inicia vetor de vertices disponiveis
   vertices.resize(nv, 0);
}

// Le grafo de entrada
void Graph::read_graph(FILE *data_file)
{
   for(int j = 0; j < nedges; j++)
   {
      int v1, v2;
      fscanf(data_file, "%d %d", &v1, &v2);
      adjmat[v1][v2] = adjmat[v2][v1] = 1;
   }
   int k = 0;
   foreach(it, vertices)
   {
      *it = k++;
   }
}

//remove um vertice da lista pelo iterador   
inline void Graph::remove_vertex(list<int>::iterator &it)
{
   it = vertices.erase(it);
   nvertex--;
}

//reconstroi a lista de vertices dispoiveis, acrescentando todos os vertices do vetor removed (tamanho n do vetor)
void Graph::rebuild_graph(int *removed, int n)
{
   list<int>::iterator t = vertices.begin();
   int i = 0;
   while(i < n && t != vertices.end())
   {
      if(*t > removed[i])
      {
         vertices.insert(t, removed[i++]);
         nvertex++;
      }
      else t++;
   }
   while(i < n)
   {
      vertices.push_back(removed[i++]);
      nvertex++;
   }
}

//remove todos todos os vertices adjacentes ao vertice 'v' acrescentando ao vetor removed e atualizando o tamanho do vetor 'num_rm'
void Graph::remove_adj(int v, int *removed, int *num_rm)
{
   list<int>::iterator it = vertices.begin();
   int d = get_degree(v);
   int offset = *num_rm;
   while(*num_rm < (offset+d) && it != vertices.end())
   {
      if(adjmat[v][*it] == 1)
      {
         removed[*num_rm] = *it;
         *num_rm = *num_rm + 1;
         remove_vertex(it);
      }
      else it++;
   }
}

//obtem o grau do vertice 'v'
inline int Graph::get_degree(int v)
{
   int ans = 0;
   foreach(it, vertices)
      if(adjmat[*it][v] == 1)
         ans++;
   return ans;
}

//caso o grafo seja formado por apenas ciclos, encontra todos os ciclos a armazena no vetor seq
int Graph::find_cycles(uint *seq)
{
   list<int> aux = vertices;
   list<int>::iterator icur = aux.begin();

   bool has_adjacent = true;
   int cur = *icur, k = 0, count = 0;
   while(aux.size() > 1)
   {
      if(!has_adjacent)
      {
         aux.erase(icur);
         icur = aux.begin();
         cur = *icur;
         count = 0;
      }

      has_adjacent = false;
      list<int>::iterator it = aux.begin();
      while(it != aux.end())
      {
         if(adjmat[cur][*it] == 1)
         {
            count++;
            has_adjacent = true;

            if(count%2) seq[k++] = cur;
            
            cur = *it;
            aux.erase(icur);
            icur = it;
            break;
         }
         it++;
      }
   }
   return k;
}

//faz uma copia simples do vetor b para o vetor a
inline void MIS::copy(uint *a, uint *b, int n)
{
   for(int i = 0; i < n; i++) a[i] = b[i];
}

//verifica se ainda esta dentro do tempo especificado 
inline bool MIS::check_time(uint tempo_maximo)
{
   clock_t aux = clock() - number_clocks;
   return ((((float)aux)/CLOCKS_PER_SEC) > tempo_maximo);
}

//retorna 1 se executou dentro do tempo especificado (< tempo_maximo), 
//0 caso contrario
uint MIS::A1(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo)
{
   //verifica se nao passou do tempo
   if(check_time(tempo_maximo))
      return 0;

   //caso base
   if(G.nvertex == 0)
   {
      *tam_ci = 0;
      return 1;
   }

   // salva iterador
   list<int>::iterator it = G.vertices.begin();
   //vertice correspondente ao iterador 
   int v = *it;
   //removed armazena os vertices removidos, util para reconstruir a lista
   int removed[G.nvertex], num_rm = 0;

   //G = G - v, v nao pertence ao MIS
   G.remove_vertex(it);
   removed[num_rm++] = v;

   *rec_calls += 1;
   if(A1(G, tam_ci, seq_vert_ci, rec_calls, tempo_maximo) == 0)
      return 0;

   //G = G - v - Adj(V), v pertence ao MIS
   it = G.vertices.begin();
   while(it != G.vertices.end())
   {
      //remove adjacentes
      if(G.adjmat[v][*it] == 1)
      {
         removed[num_rm++] = *it;
         G.remove_vertex(it);
      }
      else it++;
   }
   
   uint size;
   uint seq[G.nvertex];

   *rec_calls += 1;
   if(A1(G, &size, seq, rec_calls, tempo_maximo) == 0)
      return 0;
   

   // se size > tam_ci, vertice v pertence ao MIS
   size++;
   if(*tam_ci < size)
   {
      seq[size-1] = v;
      copy(seq_vert_ci, seq, size);
      *tam_ci = size;
   }

   // Reconstroi lista de vertices disponives do grafo
   G.rebuild_graph(removed, num_rm);
   return 1;
}

//retorna 1 se executou dentro do tempo especificado (< tempo_maximo), 
//0 caso contrario
uint MIS::A2(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo)
{
   //verifica se nao passou do tempo
   if(check_time(tempo_maximo))
      return 0;

   // caso base
   if(G.nvertex == 0)
   {
      *tam_ci = 0;
      return 1;
   }

   list<int>::iterator it;
   int removed[G.nvertex], num_rm = 0;

   // Existe algum vertice v, tal que d(v) <= 1
   it = G.vertices.begin();
   while(it != G.vertices.end())
   {
      if(G.get_degree(*it) <= 1) break;
      it++;
   }

   if(it != G.vertices.end())
   {
      //se d(v)<=1, entao v pertecence ao MIS
      int v = *it;
      G.remove_vertex(it);
      removed[num_rm++] = v;
      if(G.get_degree(v)==1)
      { 
         // d(v) = 1, remove adjacente
         G.remove_adj(v, removed, &num_rm);
      }

      *rec_calls += 1;
      if(A2(G, tam_ci, seq_vert_ci, rec_calls, tempo_maximo) == 0)
         return 0;
      seq_vert_ci[*tam_ci] = v;
      *tam_ci = *tam_ci+1;

      // reconstroi lista de vertices livres
      G.rebuild_graph(removed, num_rm);
      return 1;
   }

   // G eh formado por ciclos?
   it = G.vertices.begin();
   while(it != G.vertices.end())
   {
      if(G.get_degree(*it)!=2)
         break;
      it++;
   }
 
   if(it == G.vertices.end())
   {
      //podemos resolver em tempo polinomial
      *tam_ci = G.find_cycles(seq_vert_ci);
      return 1;
   }

   //Existe vertice v, tal que d(v)>=3

   //G = G - v, v nao pertence ao MIS
   int v = *it;
   G.remove_vertex(it);
   removed[num_rm++] = v;

   *rec_calls += 1;
   if(A2(G, tam_ci, seq_vert_ci, rec_calls, tempo_maximo) == 0)
      return 0;

   // G = G - v - Adj(v), v pertence ao MIS
   // Remove vertices adjacentes 
   G.remove_adj(v, removed, &num_rm);
      
   uint size;
   uint seq[G.nvertex];

   *rec_calls += 1;
   if(A2(G, &size, seq, rec_calls, tempo_maximo) == 0)
      return 0;

   
   // se size > tam_ci, vertice v pertence ao MIS
   size++;
   if(*tam_ci < size)
   {
      seq[size-1] = v;
      copy(seq_vert_ci, seq, size);
      *tam_ci = size;
   }
   // Reconstroi lista de vertices disponives do grafo
   G.rebuild_graph(removed, num_rm);
   return 1;
}

//retorna 1 se executou dentro do tempo especificado (< tempo_maximo), 
//0 caso contrario
uint MIS::A3(Graph &G, uint *tam_ci, uint *seq_vert_ci, uint *rec_calls, uint tempo_maximo)
{
   //verifica se nao passou do tempo
   if(check_time(tempo_maximo))
      return 0;

   //caso base
   if(G.nvertex == 0)
   {
      *tam_ci = 0;
      return 1;
   }

   list<int>::iterator it;
   int removed[G.nvertex], num_rm = 0;

   //adiciona ao MIS todos os vertices v tais que d(v)<=1
   //note que pode surgir novos vertices com essa propriedade
   //ao longo da iteracao
   *tam_ci = 0;
   it = G.vertices.begin();
   while(it != G.vertices.end())
   {
      int d = G.get_degree(*it);
      if(d <= 1)
      {
         seq_vert_ci[(*tam_ci)++] = *it;
         list<int>::iterator i = G.vertices.begin();
         if(d == 1)
         {
            while(G.adjmat[*it][*i] == 0) i++;
            removed[num_rm++] = *i;
            G.vertices.erase(i);
            G.nvertex--;
         }
         removed[num_rm++] = *it;
         it = G.vertices.erase(it);
         G.nvertex--;
      }
      else it++;
   }

   //Se a lista esta vazia termina
   if(G.vertices.empty())
   {
      G.rebuild_graph(removed, num_rm);
      return 1;
   }

   //G eh formado por cilcos ?
   it = G.vertices.begin();
   while(it != G.vertices.end())
   {
      if(G.get_degree(*it) != 2)
         break;
      it++;
   }
 
   if(it == G.vertices.end())
   {
      //G eh formado por ciclos e podemos resolver em tempo polinomial
      int mis_cycle;
      uint vert_cycle[G.nvertex];
      mis_cycle = G.find_cycles(vert_cycle);
      for(int i = *tam_ci; i < *tam_ci+mis_cycle; i++)
         seq_vert_ci[i] = vert_cycle[i-*tam_ci];
      *tam_ci += mis_cycle;
     G.rebuild_graph(removed, num_rm);
     return 1;
   }

   // Busca vertice com maior grau dentro dos disponiveis
   // Essa escolha faz diminui bastante a densidade do grafo
   // da chamada recursiva
   list<int>::iterator t = G.vertices.begin();
   int a = G.get_degree(*t);
   t++;
   while(t != G.vertices.end())
   {
      int d = G.get_degree(*t);
      if(a < d)
      {
         a = d;
         it = t;
      }
      t++;
   }

   //G = G - v, v nao pertence ao MIS
   int v = *it;
   G.remove_vertex(it);
   removed[num_rm++] = v;

   uint size;
   uint seq[G.nvertex];

   *rec_calls += 1;
   if(A3(G, &size, seq, rec_calls, tempo_maximo) == 0)
      return 0;

   // G = G - v - Adj(v), v pertence ao MIS
   // Remove vertices adjacentes 
   G.remove_adj(v, removed, &num_rm);
   int save_tam = *tam_ci;

   *rec_calls += 1;
   if(A3(G, tam_ci, seq_vert_ci + save_tam, rec_calls, tempo_maximo) == 0)
      return 0;
   
   //se *tam_ci > size, v pertence ao MIS
   (*tam_ci)++;
   if(*tam_ci > size)
   {
      *tam_ci += save_tam;
      seq_vert_ci[*tam_ci-1] = v;
   }
   else
   {
      copy(seq_vert_ci+save_tam, seq, size);
      *tam_ci = size + save_tam;
   }

   // Reconstroi lista de vertices disponives do grafo
   G.rebuild_graph(removed, num_rm);
   return 1;
}

