#!/bin/bash

for f in $(ls tests/*.in | cut -d'.' -f1)
do
   #./a.out < $f >> tests/res
   #echo $f
   ./a.out < $f'.in' > $f'.diff'
   diff $f'.out' $f'.diff'
   rm $f'.diff'
   #echo -e '\n'
done

#diff tests/ans tests/res
#rm tests/res
