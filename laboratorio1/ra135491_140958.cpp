//-----------------------------------------
// MC458 - 2s-2014
//
// Diego Luis de Souza	RA: 135491
// Ulisses Malta Santos	RA: 140958
//
// Polynomial Multiplication
//-----------------------------------------

#include <complex>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>

using namespace std;

#define EPS 1e-6
double const PI = acos(-1);

//-----------------------------------------
// Debug - writes coefficients in a file
void print_vector(int size, double C[], const char *method, const char *file_name, int flag)
{
	if(!C) 
		return;
	
	char result_name[100];
	sprintf(result_name, "result_%s_%s", method, file_name);	
	
	FILE *result_file;
	if(flag != 0)
		result_file = fopen(result_name, "a");
	else
		result_file = fopen(result_name, "w");
	
	if(result_file != NULL)
	{
		fprintf(result_file, "Size: %d\n", size);
		for(int i = 0; i < size; i++) 
			fprintf(result_file, "%.0lf ", abs(C[i]-0.0) > EPS ? C[i] : 0.0);
		fprintf(result_file, "\n");
	
		fclose(result_file);
	}
}
//-----------------------------------------

class MultPoly
{
   public:
      clock_t number_clocks;
      unsigned int number_add_quad;
      unsigned int number_mult_quad;
      unsigned int number_add_fft;
      unsigned int number_mult_fft;
		unsigned int number_add_kara;
		unsigned int number_mult_kara;

      void multpoly_fft(int n, double A[], double B[], double C[]);
      void multpoly_quad(int n, double A[], double B[], double C[]);
		void multpoly_kara(int n, double A[], double B[], double C[]);    // Recursive
		void multpoly_kara_it(int n, double A[], double B[], double C[]); // Iterative

   private:
      void sum_poly(int n, int la, double A[], int lb, double B[], double C[]);
      void mult_powerx(int n, int xn, double P[], double C[]);
      int my_log2(int n);
      unsigned rev(unsigned k, unsigned bits);
      void bit_reversal(int n, complex<double> S[], complex<double> D[]);
      void FFT(int n, int signal, complex<double> S[], complex<double> D[]);
      void karatsuba(int l, int u, double A[], double B[], double C[]);
};

int main(int argc, char *argv[])
{	
   // Missing arguments
	if(argc < 2)
	{
		printf("Argumento incorreto!!!\n");
		return 0;
	}

	FILE *data_file;
	data_file = fopen(argv[1], "r");

   if(data_file == NULL)
   {
      printf("Erro ao abrir o arquivo de dados!!!\n");
      return 0;
   }

   int number_of_pairs, i, j, sizeA, sizeB;

   fscanf(data_file, "%d", &number_of_pairs);

   // Parameters to analyze algorithms
   clock_t clocks_quad = 0;
   clock_t clocks_fft  = 0;
	clock_t clocks_kara = 0;

   MultPoly poly;
   // Process each pair of polynomials
   for (int i = 0; i < number_of_pairs; i++)
   {
      fscanf(data_file, "%d", &sizeA);
      double A[sizeA];
      for (int j = 0; j < sizeA; j++)
         fscanf(data_file, "%lf", &A[j]);

      fscanf(data_file, "%d", &sizeB);
      double B[sizeB];
      for (int j = 0; j < sizeB; j++)
         fscanf(data_file, "%lf", &B[j]);

      double C[sizeA+sizeB-1];

      // Multiplication and updating of variables
      if(sizeA == sizeB && A && B && C)
      {
			memset(C, 0, sizeof(C));
         poly.multpoly_quad(sizeA, A, B, C);
         clocks_quad += poly.number_clocks;
			
         // Debug, to verify coefficients
			// print_vector(2*sizeA-1, C, "quad", argv[1], i);
			
			memset(C, 0, sizeof(C));
         poly.multpoly_fft(sizeA, A, B, C);
         clocks_fft += poly.number_clocks;
         
         // Debug, to verify coefficients
			// print_vector(2*sizeA-1, C, "fft", argv[1], i);
			
			memset(C, 0, sizeof(C));
			poly.multpoly_kara(sizeA, A, B, C);
			clocks_kara += poly.number_clocks;

			// Debug, to verify coefficients
			// print_vector(2*sizeA-1, C, "kara", argv[1], i);
      }
   }
	
	fclose(data_file);

   // Calculate average time
   float time_quad = (((float)clocks_quad)/CLOCKS_PER_SEC)/number_of_pairs;
   float time_fft  = (((float)clocks_fft)/CLOCKS_PER_SEC)/number_of_pairs;
	float time_kara = (((float)clocks_kara)/CLOCKS_PER_SEC)/number_of_pairs;
	
	// Log file
	// <algoritmo> <arquivo> <numero_multiplicacoes> <numero_adicoes> <tempo_medio_de_execucao>

	FILE *log_file;
	log_file = fopen("ra135491_140958.log", "a");

	if(log_file != NULL)
	{
		fprintf(log_file, "quad %s %d %d %f\n", argv[1], poly.number_mult_quad, poly.number_add_quad, time_quad);
		fprintf(log_file, "kara %s %d %d %f\n", argv[1], poly.number_mult_kara, poly.number_add_kara, time_kara);		
		fprintf(log_file, "fft %s %d %d %f\n\n", argv[1], poly.number_mult_fft, poly.number_add_fft, time_fft);		
		fclose(log_file);
	}

	return 0;
}

// Sum polynomials with the same length "n" starting by "la". C = A + B
void MultPoly::sum_poly(int n, int la, double A[], int lb, double B[], double C[])
{
   for (int i = 0; i<n; ++i)
      C[i] = A[i+la] + B[i+lb];
}

// Multiply polyminal by x^n. C = P*x^n
void MultPoly::mult_powerx(int n, int xn, double P[], double C[])
{
   for (int i = xn; i < n; ++i)
      C[i] = P[i-xn];
   for (int i = 0; i < xn; ++i) 
      C[i] = 0;
}

// My_log2(n) ONLY if n is power of 2
int MultPoly::my_log2(int n)
{
   int tmp = 1;
   int count = 0;
   while (n != tmp && tmp <= n)
   {
      count++;
      tmp <<= 1;
   }
   return count;
}

// Bit reverse of k considering "bits" bits. Eg: 100 -> 001
unsigned MultPoly::rev(unsigned k, unsigned bits)
{
   unsigned ans = k;
   unsigned mask_clear = (1 << bits)-1;
   for (unsigned i = 1; i < bits; i++)
   {
      k >>= 1;
      ans <<= 1;
      ans |= k & 1;
   }
   ans &= mask_clear;
   return ans;
}

// Bit reversal permutation, from S to D
void MultPoly::bit_reversal(int n, complex<double> S[], complex<double> D[])
{
   for (int i = 0; i < n; i++)
      D[rev(i, my_log2(n))] = S[i];
}

// FFT(S, W) or FFT(S,-W) depending on the value of "signal"
// Polynomial of degree n-1, result in D[]
void MultPoly::FFT(int n, int signal, complex<double> S[], complex<double> D[])
{
   bit_reversal(n, S, D);
   int l = my_log2(n); // n is a power of 2;
   for (int s = 1; s <= l; s++)
   {
      int m = 1 << s;
      complex<double> Wm = polar(1.0, signal*(double)(2*PI)/m);
      for (int k = 0; k < n; k += m)
      {
         complex<double> W(1, 0);
         for (int j = 0; j < m/2; j++)
         {
            complex<double> t = W*D[k+j+m/2];
            complex<double> u = D[k+j];
            D[k+j] = u + t;
            D[k+j+m/2] = u - t;
            W *= Wm;
				number_add_fft += 2;
				number_mult_fft += 2;
         }
      }
   }
}

// Polynomial of dedree n-1, C = A*B, fft approach, O(n*log(n))
void MultPoly::multpoly_fft(int n, double A[], double B[], double C[])
{
   // Parameters to analyze performance
   clock_t t = clock();
   number_add_fft = 0;
   number_mult_fft = 0;
	
	// n must be power of 2
   int near_power2 = 1 << my_log2(n);
	
   // Convert to complex
   complex<double> CA[2*near_power2], CB[2*near_power2];
   for (int i = 0; i < n; i++)
   {
      CA[i] = A[i];
      CB[i] = B[i];
   }
   
   // Extend polynomial with zeros since we need 2*near_power2 - 1 points to determine C = A*B
   for (int i = n; i < 2*near_power2; i++)
   {
      CA[i] = 0.0;
      CB[i] = 0.0;
   }

   complex<double> DA[2*near_power2], DB[2*near_power2], TMP[2*near_power2], DC[2*near_power2];

   // Calculate 2*near_power2-1 points of CA and CB
   FFT(2*near_power2, 1, CA, DA);
   FFT(2*near_power2, 1, CB, DB);

   for (int i = 0; i < 2*near_power2; i++)
	{
		TMP[i] = DA[i]*DB[i];
		number_mult_fft++;
	}

   // FFT inverse (signal -1) to determine coefficients
   FFT(2*near_power2, -1, TMP, DC);

   // C[i] = DC[i]/(degree(C)+1)
   for (int i = 0; i < (2*n-1); i++)
	{
      C[i] = real(DC[i])/(2*near_power2);
		number_mult_fft++;
	}

   t = clock() - t;
   number_clocks = t;
}

// Polynomial of dedree n-1, C = A*B, straightforward approach, O(n^2)
void MultPoly::multpoly_quad(int n, double A[], double B[], double C[])
{
   // Parameters to analyze performance
   clock_t t = clock();
   number_mult_quad = 0;
   number_add_quad = 0;

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
		{
			C[i+j] += (A[i]*B[j]);
			number_add_quad++;
			number_mult_quad++;
		}

   t = clock() - t;
   number_clocks = t;
}

// Recursive karatsuba algorithm. 
// C = A[l, ...,u] * B[l, ...,u]. Suppose coefficients from "l" to "u"
void MultPoly::karatsuba(int l, int u, double A[], double B[], double C[])
{
   // Base case
   if(u==l)
	{
      C[0] = A[l]*B[l];
      number_mult_kara++;
      return;
   }

   // Auxiliary arrays
   int n = (u-l)+1;
   double D0[n], D1[n], D01[n];
   double aux1[2*n], aux2[2*n];

   // Divide and conquer
   karatsuba(l, (u+l)/2, A, B, D0);
   karatsuba((u+l)/2+1, u, A, B, D1);

   int m = n/2;
   sum_poly(m, l, A, l+m, A, aux1);
   sum_poly(m, l, B, l+m, B, aux2);
   number_add_kara += 2*(m-1);
   karatsuba(0, m-1, aux1, aux2, D01);
    
   for (int i = 0; i < 2*m-1; i++) 
      aux1[i] = D01[i] - (D0[i] + D1[i]);

   number_add_kara += 2*(2*m-1);

   // D1*x^n
   mult_powerx(2*n-1, n, D1, C);
   // (D01 - D0 - D1)*x^(n/2)
   mult_powerx(3*m-1, m, aux1, aux2);

   for (int i = 3*m-1; i < 2*n-1; i++)
      aux2[i] = 0;

   for (int i = 0; i < 2*m-1; i++)
      aux1[i] = D0[i];

   for (int i = 2*m-1; i < 2*n-1; i++)
      aux1[i] = 0;
   
   // C = D1*x^n + (D01 - D0 - D1)*x^(n/2) + D0
   sum_poly(2*n-1, 0, C, 0, aux2, C);
   sum_poly(2*n-1, 0, C, 0, aux1, C);
   number_add_kara += 2*(2*n-1);
}

// Polynomial of dedree n-1, C = A*B, karatsuba approach, O(n^log2(3))
void MultPoly::multpoly_kara(int n, double A[], double B[], double C[])
{
   // Parameters to analyze performance
   clock_t t = clock();
   number_add_kara = 0;
   number_mult_kara = 0;

	// n must be a power of 2
   int near_power2 = 1 << my_log2(n);
   double A2[near_power2], B2[near_power2];
	
   for (int i = 0; i < n; ++i)
   {
      A2[i]=A[i];
      B2[i]=B[i];
   }
   for (int i = n; i < near_power2; i++)
   {
      A2[i] = 0.0;
      B2[i] = 0.0;
   }

   // Karatsuba algorithm
   karatsuba(0, near_power2-1, A2, B2, C);

   t = clock() - t;
   number_clocks = t;
}

// Iterative karatsuba algorithm. 
// C = A * B.
void MultPoly::multpoly_kara_it(int n, double A[], double B[], double C[])
{
	// Parameters to analyze performance
   clock_t u = clock();
   number_mult_kara = 0;
   number_add_kara = 0;

	double D[n];
	int s, t;	

	for (int i = 0; i < n; i++)
	{
		D[i] = A[i]*B[i];
		number_mult_kara++;
	}

	C[0] = D[0];
	C[2*n-2] = D[n-1];

	for (int i = 1; i < (2*n-2); i++)
	{
		// s + t = i
		t = i;
		if(t >= n) {
			t = n-1;			
		}
		s = i-t;
		while (s < t)
		{		
			C[i] = C[i] + (A[s]+A[t])*(B[s]+B[t]) - D[s] - D[t];
			s++;
			t--;
			number_mult_kara++;
			number_add_kara += 5;
		}
		if(i%2 == 0) 
		{
			C[i] += D[i/2];
			number_add_kara++;	
		}
	}

	u = clock() - u;
   number_clocks = u;
}
