	// Calculate average time
	float time_quad = (((float)clocks_quad)/CLOCKS_PER_SEC)/number_of_pairs;
	float time_fft  = (((float)clocks_fft)/CLOCKS_PER_SEC)/number_of_pairs;
	float time_kara = (((float)clocks_kara)/CLOCKS_PER_SEC)/number_of_pairs;

	// Log file
	// <algoritmo> <arquivo> <numero_multiplicacoes> <numero_adicoes> <tempo_medio_de_execucao>

	FILE *log_file;
	log_file = fopen("ra135491_140958.log", "a");

	if(log_file != NULL)
	{
		fprintf(log_file, "quad %s %d %d %f\n", argv[1], poly.number_mult_quad, poly.number_add_quad, time_quad);
		fprintf(log_file, "kara %s %d %d %f\n", argv[1], poly.number_mult_kara, poly.number_add_kara, time_kara);		
		fprintf(log_file, "fft %s %d %d %f\n\n", argv[1], poly.number_mult_fft, poly.number_add_fft, time_fft);
		fclose(log_file);
	}
