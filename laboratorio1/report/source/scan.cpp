	// Missing arguments
	if(argc < 2)
	{
		printf("Argumento incorreto!!!\n");
		return 0;
	}

	FILE *data_file;
	data_file = fopen(argv[1], "r");

	if(data_file == NULL)
	{
		printf("Erro ao abrir o arquivo de dados!!!\n");
		return 0;
	}

	int number_of_pairs, i, j, sizeA, sizeB;

	fscanf(data_file, "%d", &number_of_pairs);

	// Parameters to analyze algorithms
	clock_t clocks_quad = 0;
	clock_t clocks_fft  = 0;
	clock_t clocks_kara = 0;

	MultPoly poly;
	// Process each pair of polynomials
	for (int i = 0; i < number_of_pairs; i++)
	{
		fscanf(data_file, "%d", &sizeA);
		double A[sizeA];
		for (int j = 0; j < sizeA; j++)
			fscanf(data_file, "%lf", &A[j]);

		fscanf(data_file, "%d", &sizeB);
		double B[sizeB];
		for (int j = 0; j < sizeB; j++)
			fscanf(data_file, "%lf", &B[j]);

		double C[sizeA+sizeB-1];

		// Multiplication and updating of variables
		if(sizeA == sizeB && A && B && C)
		{
			memset(C, 0, sizeof(C));
			poly.multpoly_quad(sizeA, A, B, C);
			clocks_quad += poly.number_clocks;

			// Debug, to verify coefficients
			// print_vector(2*sizeA-1, C, "quad", argv[1], i);

			memset(C, 0, sizeof(C));
			poly.multpoly_fft(sizeA, A, B, C);
			clocks_fft += poly.number_clocks;

			// Debug, to verify coefficients
			// print_vector(2*sizeA-1, C, "fft", argv[1], i);

			memset(C, 0, sizeof(C));
			poly.multpoly_kara(sizeA, A, B, C);
			clocks_kara += poly.number_clocks;

			// Debug, to verify coefficients
			// print_vector(2*sizeA-1, C, "kara", argv[1], i);
		}
	}

	fclose(data_file);
