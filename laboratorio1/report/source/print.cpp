// Debug - writes coefficients in a file
void print_vector(int size, double C[], const char *method, const char *file_name, int flag)
{
	if(!C) 
		return;

	char result_name[100];
	sprintf(result_name, "result_%s_%s", method, file_name);	

	FILE *result_file;
	if(flag != 0)
		result_file = fopen(result_name, "a");
	else
		result_file = fopen(result_name, "w");

	if(result_file != NULL)
	{
		fprintf(result_file, "Size: %d\n", size);
		for(int i = 0; i < size; i++) 
		fprintf(result_file, "%.0lf ", abs(C[i]-0.0) > EPS ? C[i] : 0.0);
		fprintf(result_file, "\n");

		fclose(result_file);
	}
}
