class MultPoly
{
   public:
		clock_t number_clocks;
		unsigned int number_add_quad;
		unsigned int number_mult_quad;
		unsigned int number_add_fft;
		unsigned int number_mult_fft;
		unsigned int number_add_kara;
		unsigned int number_mult_kara;

		void multpoly_fft(int n, double A[], double B[], double C[]);
		void multpoly_quad(int n, double A[], double B[], double C[]);
		// Recursive
		void multpoly_kara(int n, double A[], double B[], double C[]);
		// Iterative
		void multpoly_kara_it(int n, double A[], double B[], double C[]); 

   private:
      void sum_poly(int n, int la, double A[], int lb, double B[], double C[]);
      void mult_powerx(int n, int xn, double P[], double C[]);
      int my_log2(int n);
      unsigned rev(unsigned k, unsigned bits);
      void bit_reversal(int n, complex<double> S[], complex<double> D[]);
      void FFT(int n, int signal, complex<double> S[], complex<double> D[]);
      void karatsuba(int l, int u, double A[], double B[], double C[]);
};

