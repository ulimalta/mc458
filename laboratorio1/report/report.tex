\documentclass[12pt, a4paper]{article}

\usepackage[hmargin=2cm,vmargin=2cm]{geometry}
\usepackage{amsmath}
\usepackage{listings}
\usepackage[brazil]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{url}
\usepackage{array}                                                                                             \usepackage{graphicx}                                                                                          \usepackage{float}
\usepackage{tocloft}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subfig}
\usepackage{pdfpages}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{color}

\definecolor{mygray}{rgb}{0.95,0.95,0.95}
\definecolor{mygreen}{rgb}{0,0.6,0}

\lstdefinestyle{mystyle}{
  % choose the language of the code
  language=C++,
  % where to put the line-numbers       
  numbers=left,
  % the step between two line-number              
  stepnumber=1,
  % how far the line-numbers are from the code       
  numbersep=5pt,
  % choose the background color. 
  backgroundcolor=\color{mygray},  
  % show spaces adding particular underscores  
  showspaces=false,  
  % underline spaces within strings             
  showstringspaces=false,
  % show tabs within strings adding particular underscores         
  showtabs=false,
  % sets default tabsize to x spaces                 
  tabsize=2,
  % sets the caption-position to bottom                      
  captionpos=b,
  % sets automatic line breaking                   
  breaklines=true,
  % sets if automatic breaks should only happen at whitespace                
  breakatwhitespace=true,
  % show the filename of files included with \lstinputlisting;         
  %title=\lstname,
  %commentstyle=\color{blue},
  %stringstyle=\color{magenta},  
  %keywordstyle=\color{brown},
  %identifierstyle=\color{blue},    
  numberstyle=\color{black},
  frame=single,        
}
\lstset{style=mystyle}

\begin{document}

\begin{titlepage}

\title{\textbf{MC458 - Projeto e Análise de Algoritmos I}}
\date{}
\clearpage\maketitle
\setcounter{page}{0}
\thispagestyle{empty}

\begin{center}
\textbf{\Large Prática de laboratório 1 - Multiplicação de Polinômios}
\end{center}
\
\
\author{
\begin{center}
Diego Luis de Souza - RA 135491\\
Ulisses Malta Santos - RA 140958\\
\end{center}

\null\vfill
\begin{center}
\textbf{Instituto de Computação (IC) - UNICAMP}
\end{center}

\begin{center}
\date{22/09/2014}
\end{center}
}

\end{titlepage}

\newpage

\tableofcontents

\newpage

\section{Introdução}
%\url{http://www.ic.unicamp.br/~rdahab/cursos/mc458/2014-2s/Welcome_files/labs.pdf}
%Ver item b)
Multiplicar dois polinômios de forma eficiente é uma questão importante para uma variedade de aplicações, desde o processamento de sinais até a criptografia. Este trabalho busca analisar e comparar experimentalmente três métodos diferentes para multiplicar polinômios, são eles: o método direto ($O(n^2)$), o método de Karatsuba ($O(n^{\log_{2}3})$) e o método da transformada rápida de Fourier ($n*log(n)$). Para observar a diferença de complexidade entre os métodos, implementamos os três algoritmos citados na linguagem C++ e os testamos com polinômios de diferentes tamanhos para verificar qual o melhor algoritmo em cada caso. Os resultados obtidos são apresentados nas seções seguintes desse relatório.

\section{Modelagem do Problema}
O polinômio $P(x)=\sum\limits_{i=0}^{n-1} a_{i}x^i$ pode ser representado univocamente utilizando um vetor de coeficientes $a=(a_{0}, a_{1}, ..., a_{n-1})$ ou um vetor de pontos $\{(x_{0},y_{0}), (x_{1}, y_{1}), ..., (x_{n-1}, y_{n-1})\}$ desde que $x_{i}\neq x_{j}$ para $i\neq j$. De acordo com essa última representação temos a seguinte equação.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.3]{img/matrix.png}\\
Eq. 1
\end{center}
\end{figure}

A matriz da esquerda, que denotaremos por $V(x_{0}, x_{1}, ..., x_{n-1})$, é conhecida como matriz de Vandermonde. Pode-se provar que seu determinante é dado por $\prod\limits_{0\leq i < j \leq n-1} (x_{j}-x_{i})$, que é diferente de zero pois $x_{i}\neq x_{j}$ para $i\neq j$. Como o determinante é diferente de zero podemos calcular a inversa da matriz $V$ e determinar os coeficientes através da equação $a=V(x_{0}, x_{1}, ..., x_{n-1})^{-1}y$, o que prova a unicidade da representação por vetor de pontos.

Para essa atividade de laboratório as entradas são polinômios na representação por coeficientes de acordo com o enunciado da tarefa. Os polinômios serão armazenados em arquivos. Na primeira linha de cada arquivo, há um inteiro que corresponde ao número de pares $<p>$  de polinômios a serem multiplicados. Em seguida, temos $2~*<p>$ linhas, cada duas linhas correspondem aos coeficientes dos dois polinômios a serem multiplicados. Cada linha que representa um polinômio possui um primeiro inteiro $<nA_i>$ (resp. $<nB_i>$) indicando o número de termos do polinômio $A_i$ (resp. $B_i$) seguido pelos coeficientes dos termos, começando pelo termo de grau 0 até o termo de grau $<nA_i-1>$ (resp. $<nB_i-1>$).

Nas subseções a seguir cada uma das abordagens utiliza uma das representações mencionadas para solucionar o problema.

\subsection{Multiplicação em tempo quadrático}

Este método representa a maneira mais simples e direta para se multiplicar dois polinômios. Dados dois polinômios $A(x)=\sum\limits_{i=0}^{n-1} a_{i}x^i$ e $B(x)=\sum\limits_{i=0}^{n-1} b_{i}x^i$ de grau $n-1$ e, considerando sua representação por coeficientes, podemos obter o polinômio $C(x)=\sum\limits_{i=0}^{2n-2} c_{i}x^i$ de grau $2n-2$, produto de $A(x)$ por $B(x)$, realizando apenas uma operação distributiva onde multiplicamos todos os coeficientes do polinômio $A(x)$ por todos os coeficientes do polinômio $B(x)$ e então combinamos os resultados obtidos dessa maneira para agrupar termos que representam os coeficientes de uma mesma potência de $x$. Dessa modo, temos que $C(x)=\sum\limits_{i=0}^{n-1}\sum\limits_{j=0}^{n-1} a_{i}b_{j}x^{i+j}$ e o algoritmo é obtido diretamente utilizando dois comandos de repetição aninhados.

Como podemos perceber, este método realiza muitas operações e por isso pode ser muito lento quando o grau dos polinômios multiplicados é muito grande. No entanto, esse algoritmo pode ser suficiente para aplicações que trabalhem com polinômios de grau pequeno e ainda pode ficar bem rápido quando é compilado com otimizações.


\subsection{Multiplicação utilizando o Algoritmo de Karatsuba}
O algoritmo karatsuba usa representação por coeficientes e a ideia principal é reduzir o número de multiplicações no algoritmo, já que uma operação de multiplicação em geral é mais custosa para o processador. Sejam  $A(x)=\sum\limits_{i=0}^{n-1} a_{i}x^i$ e $B(x)=\sum\limits_{i=0}^{n-1} b_{i}x^i$ os polinômios que queremos multiplicar e supondo $n$ potência de 2 temos as seguintes representações:

\begin{center}
$A(x)=\sum\limits_{i=0}^{n-1} a_{i}x^i = A_{u}(x)x^{n/2}+A_{l}(x)$\\
$B(x)=\sum\limits_{i=0}^{n-1} b_{i}x^i = B_{u}(x)x^{n/2}+B_{l}(x)$\\
$A*B=A_{u}*B_{u}x^n+(A_{u}*B_{l} + A_{l}*B_{u})x^{n/2}+A_{l}*B_{l}$
\end{center}

Indutivamente teríamos 4 chamadas recursivas para calcular $A*B$ reduzindo $n$ pela metade em cada uma delas. Assim, o número de operações teria complexidade $O(4^{log_{2}{n}})=O(n^{log_{2}{4}})=O(n^2)$. Sejam $D_{1}=A_{u}*B_{u}, D_{0,1}=(A_{u}+A_{l})(B_{l}+B_{u})$ e $D_{0}=A_{l}*B_{l}$. O coeficiente de $x^{n/2}$ pode ser calculado utilizando $D_{1}$, $D_{0}$ e $D_{0,1}$, pois $A_{u}*B_{l} + A_{l}*B_{u}=D_{0,1}-D_{0}-D_{1}$. Dessa forma, conseguimos obter $A*B$ com apenas 3 chamadas recursivas e a complexidade do número de operações fica $O(3^{log_{2}{n}})=O(n^{log_{2}{3}})=O(n^{1.585})$

\begin{center}
$A*B=D_{1}x^n+(D_{0,1}-D_{0}-D_{1})x^{n/2}+D_{0}$
\end{center}

O Algoritmo recursivo que calcula $C=A*B$ é mostrado a seguir.\\

\noindent$RECURSIVE\_KA(A, B)$
\begin{algorithmic}[1]
\State $n = max(degree(A), degree(B))+1$  ~~~~~~~~~~~~~~~~~~~~~~~~   // n é potência de 2
\If {$n == 1$}
    \Return $A*B$
\EndIf
\State $A(x)=A_{u}(x)x^{n/2}+A_{l}(x)$
\State $B(x)=B_{u}(x)x^{n/2}+B_{l}(x)$
\State $D_{0}=RECURSIVE\_KA(A_{l}, B_{l})$
\State $D_{1}=RECURSIVE\_KA(A_{u}, B_{u})$
\State $D_{0,1}=RECURSIVE\_KA(A_{u}+A_{l}, B_{l}+B_{u})$\\
\Return {$D_{1}x^n+(D_{0,1}-D_{0}-D_{1})x^{n/2}+D_{0}$}
\end{algorithmic}
\

\
Sejam agora $P_{i}=a_{i}b_{i}$ e $P_{s,t}=(a_{s}+a_{t})*(b_{s}+b_{t})$. Uma versão interativa pode ser obtida se observarmos que $C(x)=A(x)*B(x)=\sum\limits_{i=0}^{2n-2} c_{i}x^i$ pode ser calculado por\\

\noindent{$c_{0}=P_{0}$}\\
$c_{2n-2}=P_{n-1}$\\
$c_{i}=\left\{
\begin{array}{c l}      
    \sum\limits_{s+t=i;t>s\geq 0} P_{s,t}-\sum\limits_{s+t=i;n>t>s\geq 0} (P_{s}+P_{t}) & i~impar,~ 0<i<2n-2\\
    \sum\limits_{s+t=i;t>s\geq 0} P_{s,t}-\sum\limits_{s+t=i;n>t>s\geq 0} (P_{s}+P_{t})+P_{i/2}& i~par,~ 0<i<2n-2
\end{array}\right.$\\
\

\
A corretude desse algoritmo iterativo está na página 5 da referência 3. Nessa atividade de laboratório, para o arquivo de log entregue, consideramos a versão recursiva.

\subsection{Multiplicação utilizando a Transformada Rápida de Fourier (FFT)}

Como mostramos na introdução dessa seção, utilizando a representação com vetor de pontos podemos determinar os coeficientes através da Eq. 1. No entanto, queremos obter $C=A*B$ onde $A(x)=\sum\limits_{i=0}^{n-1} a_{i}x^i$ e $B(x)=\sum\limits_{i=0}^{n-1} b_{i}x^i$, e seria necessário calcular a inversa de uma matriz bem como avaliar $A$ e $B$ em $2n-1$ pontos, o que pode ser muito custoso do ponto de vista de processamento. Uma primeira ideia seria tentar avaliar um polinômio em $d$ pontos de uma forma eficiente.

Dado um polinômio $P(x)=\sum\limits_{i=0}^{n-1} a_{i}x^i$ podemos escrevê-lo como 
$P(x)=P_{A}(x^2)+xP_{I}(x^2)$ onde $P_{A}$ agrupa os coeficientes de $P$ de grau par e $P_{I}$ agrupa os de grau ímpar. Por exemplo, podemos escrever $P(x)=a_{0}+a_{1}x+a_{2}x^2+a_{3}x^3+a_{4}x^4+a_{5}x^5$ como
$P(x)=(a_{0}+a_{2}x^2+a_{4}x^4) + x(a_{1}+a_{3}x^2+a_{5}x^4)$. Como $z^2=(-z)^2$ se escolhermos os pontos
$\pm z_{0}, \pm z_{1}, ..., \pm z_{n/2 -1}$ (n potência de 2) conseguimos obter $P(z)$ e $P(-z)$ simultaneamente. Portanto, indutivamente podemos avaliar $P$ em $n$ pontos da seguinte forma.

\begin{enumerate}
\item Calculo $P_{A}$ em $n/2$ pontos: $z_{0}^2, z_{1}^2, ..., z_{n/2 -1}^2$
\item Calculo $P_{I}$ em $n/2$ pontos: $z_{0}^2, z_{1}^2, ..., z_{n/2 -1}^2$
\item Calculo $P(z_{i})=P_{A}(z_{i}^2)+z_{i}P_{I}(z_{i}^2)$
\end{enumerate}

Considere $C(n)$ a complexidade que indica o número de operações. Temos uma operação de adição e outra de multiplicação sendo realizadas $n$ vezes e duas chamadas recursivas. Logo

\begin{center}
$C(n)=2C(n/2)+2n \Rightarrow C(n)=2nlog(n)+O(n) \Rightarrow \Theta(nlog(n))$
\end{center}

Para conseguirmos aplicar esse método indutivo em $P_{A}$ e $P_{I}$ seria necessário que os pontos 
$z_{0}^2, z_{1}^2, ..., z_{n/2 -1}^2$ também fossem da forma $\pm z$, o que não é possível no campo dos 
reais. Para resolver esse problema uma ideia interessante é escolher as $n$ raízes da unidade 
$\omega_{j}=e^{i2\pi j/n}$, $j=0, 1, ..., n-1$ já que 

\begin{enumerate}
\item $\omega_{j}^2 = e^{i\frac{2\pi}{\frac{n}{2}}j}$ ainda é raiz $n/2$-ésima da unidade.
\item $-\omega_{j} = \omega_{k} = e^{\pi i}e^{i\frac{2\pi}{n}j}=e^{i\frac{2\pi }{n}(n/2+j)} \Rightarrow
k=\frac{n}{2}+j$ e $-z_{j}$ também é raiz da unidade que é justamente o que desejávamos.
\end{enumerate}

Com essas informações podemos elaborar um algoritmo recursivo que avalia o polinômio $a$ de grau $n-1$ nas $n$ raízes da unidade e coloca o resultado em $y$.\\

\noindent$RECURSIVE\_FFT(a)$
\begin{algorithmic}[1]
\State $n = a.length$  ~~~~~~~~~~~~~~~~~~~~~~~~   // n é potência de 2
\If {$n == 1$}
    \Return $a$
\EndIf
\State $\omega_{n}=e^{{2\pi i}/n}$
\State $\omega=1$
\State $a^{[0]}=(a_{0}, a_{2}, ..., a_{n-2})$
\State $a^{[1]}=(a_{1}, a_{3}, ..., a_{n-1})$
\State $y^{[0]}=RECURSIVE-FFT(a^{[0]})$
\State $y^{[1]}=RECURSIVE-FFT(a^{[1]})$
\For{$k=0$ \textbf{to} $n/2-1$}
\State $y_{k}=y_{k}^{[0]}+\omega y_{k}^{[1]}$
\State $y_{k+n/2}=y_{k}^{[0]}-\omega y_{k}^{[1]}$
\State $\omega=\omega \omega_{n}$ \EndFor
\Return {y}
\end{algorithmic}
\

\
Nessa atividade de laboratório, no entanto, preferimos implementar uma versão iterativa desse algoritmo
para evitar alocações sucessivas nas chamadas recursivas. Para transformar para forma iterativa uma ideia é tentar analisar a árvore das chamadas recursivas.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.3]{img/tree.png}
\caption{Árvore das chamadas recursivas}
\end{center}
\end{figure}

No exemplo da figura 1 podemos obter a ordem $a_{0}, a_{4}, a_{2}, a_{6}, a_{1}, a_{5}, a_{3}$ e $a_{7}$ e fazer uma abordagem bottom-up tentando "imitar" a volta da recursão. Essa ordem é obtida fazendo o reverso da representação em bits de um número considerando $log(n+1)$ bits onde $n$ é o grau do polinômio. Por exemplo, um polinômio de grau 7 consideramos 3 bits e $1=001$ vira $4=100$. Seguindo esse raciocínio, substituímos as chamadas recursivas por loops e implementamos o seguinte algoritmo que retorna em $A$ o polinômio $a$ avaliado em $n$ pontos.\\

\noindent$ITERATIVE\_FFT(a, signal)$
\begin{algorithmic}[1]
\State $BIT\_REVERSE\_COPY(a, A)$
\State $n = a.length$  ~~~~~~~~~~~~~~~~~~~~~~~~   // n é potência de 2
\For{$s=1$ \textbf{to} $log(n)$}
\State $m=2^s$
\State $\omega_{m}=e^{signal({2\pi i}/m)}$~~~~~~~~~~~~~~   // se $signal=-1$ calcula FFT inversa
\For{$k=0$ \textbf{to} $n-1$ \textbf{by} $m$ }
\State $\omega=1$
\For{$j=0$ \textbf{to} $m/2-1$}
\State $t=\omega A[k+j+m/2]$
\State $u=A[k+j]$
\State $A[k+j]=u+t$
\State $A[k+j+m/2]=u-t$
\State $\omega=\omega \omega_{m}$ \EndFor
\EndFor
\EndFor
\Return {A}
\end{algorithmic}
\

\
A chamada ao $BIT\_REVERSE\_COPY(a, A)$ coloca os bits na ordem reversa dos $n$ elementos do vetor $a$, sendo que em cada operação de reverter é considerado $log(n)$ bits. Dessa forma, esse procedimento é executado em $nlog(n)$.
Considerando agora os loops entre as linhas 3 e 13, seja $L(n)$ o número de operações. O \textbf{for} loop das linhas 6-13 itera $n/m=n/2^{s}$ vezes para cada valor de $s$, e o loop mais interno das linhas 8-13 itera $m/2=2^{s-1}$ vezes. Logo

\begin{center}
$L(n)=\sum\limits_{s=1}^{log(n)} \frac{n}{2^s}2^{s-1} = \sum\limits_{s=1}^{log(n)} \frac{n}{2} = \Theta(nlog(n))$
\end{center}

Já conseguimos a representação de $C=A*B$ em vetor de pontos em $\Theta(nlog(n))$ através da FFT, mas queremos obter os coeficientes e para isso seria necessário calcular a inversa de uma matriz (Eq. 2). No entanto, pode-se mostrar que $V(\omega _{0}, \omega _{1}, ..., \omega _{n-1})^{-1}=\frac{1}{n} V(\omega _{0}^{-1}, \omega _{1}^{-1}, ..., \omega _{n-1}^{-1})$, ou seja, basta utilizar a FFT inversa ($signal=-1$). A Figura 2 resume a modelagem do problema.

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{img/eq2.png}\\
Eq. 2
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{img/ra.png}
\caption{Multiplicação rápida de polinômios}
\end{figure}

O algoritmo foi implementado para o grau do polinômio sempre sendo potência de 2. Quando, no entanto, isso não for verdade completamos os polinômios de entrada com zeros de modo que seu grau se torne potência de 2. Isso altera a complexidade apenas por uma constante já que, se $n$ for o grau do polinômio, entre $n$ e $2n$ sempre existe uma potência de 2.
Um resumo dos passos para obter $C=A*B$ é
\begin{enumerate}
\item Sejam $A(x)=\sum\limits_{i=0}^{n-1} a_{i}x^i$,  $B(x)=\sum\limits_{i=0}^{n-1} b_{i}x^i$ e $C(x)=A(x)*B(x)=\sum\limits_{i=0}^{2n-2} c_{i}x^i$
\item Calculo $A_{i}=A(\omega ^i)$. $ITERATIVE\_FFT(A, 1) \Rightarrow$ retorna: $[A_{0}, ..., A_{2n-2}]$
\item Calculo $B_{i}=B(\omega ^i)$. $ITERATIVE\_FFT(B, 1) \Rightarrow$ retorna: $[B_{0}, ..., B_{2n-2}]$
\item Calculo $C_{i}=A_{i}B_{i} \Rightarrow$ retorna: $[C_{0}, ..., C_{2n-2}]$
\item Calculo FFT inversa $ITERATIVE\_FFT(C, -1) \Rightarrow$ retorna coeficientes multiplicado por $n$: $[nc_{0}, ..., nc_{2n-2}]$
\item $C(x)=\sum\limits_{i=0}^{2n-2} c_{i}x^i$
\end{enumerate}

\section{Estrutura do programa}
Os algoritmos para multiplicação de polinômios descritos acima foram desenvolidos em C++ e podem ser encontrados no arquivo ra135491\_140958.cpp. Um Makefile também foi feito para facilitar a compilação do código.\\

\lstinputlisting[title = Makefile]{source/Makefile.txt}

Depois de compilado de acordo com as instruções acima, o programa pode ser executado. É nesse momento que o arquivo de dados com os polinômios a serem multiplicados deve ser especificado. Esse arquivo de dados deve seguir o formato descrito na seção de modelagem do problema para que o programa funcione corretamente. \\

\lstinputlisting[title = Exemplo de execução do programa]{source/Exemplo.txt}

Para facilitar o entendimento do código, criamos uma classe chamada MultPoly que agrega os três algoritmos implementados para a multiplicação de polinômios e ainda possui atributos que registram o número de operações realizadas em cada método para a geração do arquivo de log. As funções privadas são apenas auxiliares. \\

\lstinputlisting[title = Classe MultPoly]{source/classe.cpp}

O programa recebe o nome do arquivo de dados via argv e então abre este arquivo para leitura. Os polinômios são escaneados do arquivo de dados aos pares e, sempre que um par é escaneado, os três algoritmos de multiplicação implementados são chamados para multiplicar esses dois polinômios. As funções que implementam os algoritmos de multiplicação são multpoly\_quad, multpoly\_kara e multpoly\_fft. Essas funções recebem como parâmetro o tamanho dos vetores a serem multiplicados (consideramos que esses vetores tem sempre o mesmo tamanho, isto é, que os polinômios a serem multiplicados tem o mesmo grau), os dois vetores a serem multiplicados e um terceiro vetor que irá armazenar o resultado da multiplicação. Consideramos que esse vetor que irá conter o resultado final já está com todos os seus elementos zerados quando a função de multiplicação foi chamada. Cada função de multiplicação registra o número de operações de adição e multiplicação que realizou ao ser executada. Essas funções ainda contabilizam o tempo que foi necessário para multiplicar cada par de polinômios. \\

\lstinputlisting[title = Scan dos polinômios e chamada das funções de multiplicação]{source/scan.cpp}

Esse processo é repetido até que todos os pares de polinômios contidos no arquivo de entrada sejam multiplicados. O programa ainda possui uma função de debug chamada de print\_vector que, caso suas chamadas sejam descomentadas do código fonte, gera arquivos com os resultados de cada método de multiplicação (ex: result\_quad\_dados1000.txt), que podem ser usados para verificar a corretude dos algoritmos implementados, visto que independentemente do método utilizado o resultado da multiplicação de dois polinômios deve ser sempre o mesmo. \\

\lstinputlisting[title = Imprime os resultados obtidos]{source/print.cpp}

Após multiplicar todos os polinômios, um arquivo de log chamado ra135491\_140958.log é gerado com as estatísticas coletadas de cada algoritmo durante a execução do programa. Ele registra dados como o número de adições e multiplicações, além do tempo médio gasto por cada algoritmo para multiplicar um par de polinômios. Se um arquivo de log já existir no diretório, os novos resultados são colocados no final desse arquivo para que os resultados anteriores não sejam perdidos. Se o arquivo não existir, um novo arquivo é criado. Este arquivo de log segue o padrão especificado no enunciado desse laboratório. \\

\lstinputlisting[title = Gerando arquivo de log]{source/log.cpp}

\section{Resultados}
Os gráficos a seguir resumem os resultados que obtivemos com essa atividade de laboratório. Comparamos o tempo de execução para multiplicação de polinômios utilizando três algoritmos de complexidades diferentes em função do número de termos do polinômio. O primeiro gráfico mostra os resultados para diversos pontos enquanto que o segundo busca mostrar quando as curvas se interceptam.

\begin{figure}[H]
\centering
\includegraphics[width=140mm]{graphics/mult_algorithms.pdf}
\caption{Resultados para multiplicação do polinômios utilizando os três algoritmos citados}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=140mm]{graphics/mult_algorithms_zoom.pdf}
\caption{Zoom dos resultados para visualizar quando as curvas se interceptam}
\end{figure}

Os resultados dos arquivos de log gerados a partir dos polinômios dados no enunciado são mostrados a seguir. O primeiro se refere à implementação recursiva do karatsuba e o segundo à implementação iterativa. \\


\lstinputlisting[title = Arquivo de log utilizando a versão recursiva do karatsuba]{source/log.log}
\lstinputlisting[title = Arquivo de log utilizando a versão iterativa do karatsuba]{source/logt.log}

Os gráficos acima foram obtidos a partir da multiplicação de polinômios cujo número de termos era uma potência de 2 considerando o algoritmo de karatsuba recursivo. Note que para o karatsuba recursivo ser sempre mais rápido que o quadrático precisamos de polinômios suficientemente grandes, pois a extensão do polinômio para potência de 2 aumenta a constante multiplicativa de complexidade. Como os dados 1000, 5000 e 10000 do laboratório estão razoavelmente longes de potências de 2, para esses dados o algoritmo quadrático ainda é mais rápido que o karatsuba recursivo, o que não acontece para potências de 2 como o gráfico mostra. Após plotados os pontos experimentais obtidos, plotamos também as curvas teóricas esperadas. 

%Através dos gráficos e dos dados gerados no arquivo de log, podemos perceber que o algoritmo da FFT é sempre mais rápido que os outros dois quando multiplicamos dois polinômios com um número suficientemente grande de termos. Quando comparamos o algoritmo quadrático com o karatsuba recursivo, o quadrático é mais rápido para polinômios grandes e o contrário acontece quando o comparamos com o karatsuba iterativo.

Para geração dos arquivos de log e compilação do programa foi utilizado uma máquina com Ubuntu 14.04, processador Intel(R) Core(TM) i5-3230M CPU @ 2.60GHz com 4GiB de memória e g++ versão 4.8.2. 

\section{Conclusão}
Após implementar e testar os três algoritmos para a multiplicação de polinômios (quadrático, karatsuba e FFT), pudemos concluir que existem grandes diferenças de performance entre os três métodos analisados. O algoritmo quadrático é mais simples e bem fácil de implementar, porém é o que apresenta o menor desempenho dos três algoritmos pois realiza muitas operações. Esse algoritmo tem uma complexidade $O(n^2)$ e, apesar do grande número de operações realizadas, pode ficar bem eficiente quando compilado com otimizações e ainda pode ser suficiente para aplicações que precisam multiplicar polinômios de grau pequeno. Já o algoritmo de karatsuba, que busca trocar operações de multiplicação por operações de adição sempre que possível, pode ser implementado tanto de forma recursiva quanto de forma iterativa. Em sua forma recursiva, o algoritmo implementado precisa que o número de coeficientes dos polinômios a serem multiplicados seja uma potência de 2. Caso os polinômios a serem multiplicados não atendam esse requisito, eles são expandidos para a potência de 2 mais próxima, o que aumenta a constante de complexidade do algoritmo (embora assintoticamente ambos iterativo e recursivo tenham a mesma complexidade), deixando-o, em alguns casos, um pouco mais lento que a sua implementação iterativa. Devido a essa expansão, polinômios com o número de coeficientes próximo de uma mesma potência de 2 levam um tempo parecido para serem multiplicados. A implementação iterativa não tem nenhuma restrição quanto ao número de coeficientes dos polinômios. Quando aumentamos o tamanho dos polinômios, esse algoritmo já se mostra mais eficiente do que o algoritmo quadrático. O algoritmo de karatsuba tem uma complexidade $O(n^{log_{2}(3)})$. O algoritmo da tranformada rápida de Fourier (FFT) é de longe o mais rápido dos três algoritmos implementados, sendo que a diferença na performance fica bem evidente conforme o tamanho dos polinômios aumenta. Esse último algoritmo também possui implementações recursiva e iterativa. Esse método é o mais complexo dos três e se baseia na avaliação dos polinômios nas diferentes raízes complexas da unidade e, assim como no caso do karatsuba, também expande o número de coeficientes dos polinômios para a potência de base 2 mais próxima. A complexidade do algoritmo da FFT é $O(n*logn).$

\begin{thebibliography}{99}
\bibitem{}
T.H. Cormen, C.E. Leiserson, R.L. Rivest, C. Stein,  Introduction to Algorithms, 3rd edition,  MIT Press, 2009: 898-925.
\bibitem{}
\url{http://www.ic.unicamp.br/~rdahab/cursos/mc458/2014-2s/Welcome_files/slidesManuscritosArnaldo/448c.pdf}. Acessado: 29/09/2014

\bibitem{}
\url{https://eprint.iacr.org/2006/224.pdf}. Acessado: 29/09/2014
%sample bibliography
%\bibitem{ALM1} 
%Bonwell, C. and Eison, J. (1991): Active Learning: creating excitement in the classroom.
%{\it ASHEERIC Higher Education Report Number 1}, Washington DC, USA

\end{thebibliography}

\end{document}
