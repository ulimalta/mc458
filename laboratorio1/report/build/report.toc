\select@language {brazil}
\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{2}
\contentsline {section}{\numberline {2}Modelagem do Problema}{2}
\contentsline {subsection}{\numberline {2.1}Multiplica\IeC {\c c}\IeC {\~a}o em tempo quadr\IeC {\'a}tico}{2}
\contentsline {subsection}{\numberline {2.2}Multiplica\IeC {\c c}\IeC {\~a}o utilizando o Algoritmo de Karatsuba}{3}
\contentsline {subsection}{\numberline {2.3}Multiplica\IeC {\c c}\IeC {\~a}o utilizando a Transformada R\IeC {\'a}pida de Fourier (FFT)}{4}
\contentsline {section}{\numberline {3}Estrutura do programa}{7}
\contentsline {section}{\numberline {4}Resultados}{11}
\contentsline {section}{\numberline {5}Conclus\IeC {\~a}o}{13}
