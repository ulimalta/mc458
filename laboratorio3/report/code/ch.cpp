inline void choose_pivot(int *v, int n, int *pivots, int npivot)
{
	//considering there are enough elements in v
	for(int i = 0; i < 2*npivot+1; i++)
	{
		pivots[i] = rand()%(n-3) + 2;
		//check if pivots[i] is already chosen
		while(1)
		{
			bool was_chosen = false;
			for(int t = 0; t < i; ++t)
			{
				if(pivots[t] == pivots[i])
				{
					was_chosen = true;
					break;
				}
			}
			if(!was_chosen) break;
			pivots[i] = rand()%(n-3) + 2;
		}
		int index = pivots[i];
		int j = i-1;
		// puts element in the right position
		while(j >= 0 && v[pivots[j]] > v[index])
		{
			pivots[j+1] = pivots[j];
			j--;
			g_num_comparacoes++;
		}
		pivots[j+1] = index;
		g_num_comparacoes++;
	}
}
