#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <memory.h>

#include "quicksort.h"

int g_num_comparacoes;
int g_num_trocas;
int g_T_ins_sort;

long long delta_time;

/* Imprime um vetor */
static void print_arr(const int v[], int n) 
{
	for(int i = 0; i < n; i++) 
	{
		printf("%d ", v[i]);
	}
	printf("\n");
}

static void print_log(int v[], int size, const char *funct, const char *input_file, int option, int position)
{
	if(!v) 
		return;

	int i;
	char result_name[100];
	sprintf(result_name, "ra135491_140958.log");	

	FILE *result_file;
	result_file = fopen(result_name, "a");

	if(result_file != NULL)
	{
		/* ra135491 <nome do algoritmo executado> <nome do arquivo de entrada> <numero do vetor dentro do arquivo> <tamanho do vetor> */
		fprintf(result_file, "ra135491_140958 %s %s %d %d\n", funct, input_file, position, size);	
		for(i = 0; i < size; i++) 
			fprintf(result_file, "%d ", v[i]);
		fprintf(result_file, "\n\n");	
		fclose(result_file);
	}
}

static void run_test_case(int v[], int n, QS_func_t qs_func, const char *funct, const char *input_file, int option, int position) 
{
	int v_[n];  
	struct timeval t_s, t_e;

	memcpy(v_, v, sizeof(v[0]) * n);

	/* Zera variaveis globais */
	//g_num_comparacoes = 0;
	//g_num_trocas = 0;
	g_T_ins_sort = 0;  

	/* Executa o algoritmo e mede o tempo */
	gettimeofday(&t_s, NULL);
	qs_func(v_, n);
	gettimeofday(&t_e, NULL);

	print_log(v_, n, funct, input_file, option, position);

	long long elapsed = (t_e.tv_sec - t_s.tv_sec) * 1000000 + (t_e.tv_usec - t_s.tv_usec);
	delta_time += elapsed;
	/* segundos - milisegundos - microsegundos */
	// printf("%llds %.3lldm %.3lldu, %d, %d, %d\n", elapsed / 1000000, (elapsed / 1000) % 1000, elapsed % 1000, g_num_comparacoes, g_num_trocas, g_T_ins_sort);
}

static void run_test_set(FILE *fp, int n_test_cases, int len_test_case, QS_func_t qs_func, int funct, const char *input_file) 
{
	const char *funct_names[] = {"Quicksort1", "Quicksort2", "Quicksort3", "QuicksortH1", "QuicksortH2", "QuicksortH3"};
	const char *f_names[] = {"quicksort1", "quicksort2", "quicksort3", "quicksortH_1", "quicksortH_2", "quicksortH_3"};
	int v[len_test_case];

	delta_time = 0;
  
	for (int k = 0; k < n_test_cases; k++) 
	{
		for (int i = 0; i < len_test_case; i++) 
		{
			if (fscanf(fp, "%d", &v[i]) != 1) {
				fprintf(stderr, "Formato invalido");
				exit(1);
			}      
		}
		//print_arr(v, len_test_case);
		run_test_case(v, len_test_case, qs_func, funct_names[funct], input_file, funct, k+1);
	}

	delta_time /= n_test_cases;
	g_num_comparacoes /= n_test_cases;
	g_num_trocas /= n_test_cases;

	/* <algoritmo> <numero de comparacoes> <numero de trocas> <tempo medio de execucao (segundos - milisegundos - microsegundos)> */
	if(funct < 3)
		printf("%s %d %d %.3llds %.3lldms %.3lldus\n\n", f_names[funct], g_num_comparacoes, g_num_trocas, delta_time / 1000000, (delta_time / 1000) % 1000, delta_time % 1000);
	/* <algoritmo> <numero de comparacoes> <numero de trocas> <tamanho maximo para aplicar o insertion sort> <tempo medio de execucao (segundos - milisegundos - microsegundos)> */
	else
		printf("%s %d %d %d %.3llds %.3lldms %.3lldus\n\n", f_names[funct], g_num_comparacoes, g_num_trocas, g_T_ins_sort, delta_time / 1000000, (delta_time / 1000) % 1000, delta_time % 1000);
}

int main (int argc, char **argv) 
{  
	if (argc < 2) 
	{
		printf("Número inválido de argumentos\n");
		printf("Uso: ./lab03 arq_dados.txt\n");
		return 1;
	}

	QS_func_t qs_funcs[] = {Quicksort1, Quicksort2, Quicksort3, QuicksortH1, QuicksortH2, QuicksortH3};

	g_num_comparacoes = 0;
	g_num_trocas = 0;
	g_T_ins_sort = 0;

	/* Roda cada algoritmo sobre todos os casos de teste contidos no arquivo */
	for (uint i = 0; i < sizeof(qs_funcs)/sizeof(qs_funcs[0]); i++) 
	{
		FILE *fp = fopen(argv[1], "r");
		if (!fp) 
		{
			fprintf(stderr, "Não foi possível abrir o arquivo de dados \"%s\".\n", argv[1]);
			return 1;
		}    
		int n_test_cases = 0;
		int len_test_case = 0;

		if (fscanf(fp, "%d %d", &n_test_cases, &len_test_case) != 2) 
		{
			fprintf(stderr, "Formato invalido.\n");
			return 1;
		}

		/* escolhe um quicksort e o executa para todos os vetores do arquivo de entrada */
		run_test_set(fp, n_test_cases, len_test_case, qs_funcs[i], i, argv[1]);
		fclose(fp);
	}

	return 0;
}

