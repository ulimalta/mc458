#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <algorithm>
#include <cassert>
#include <cstring>

#include "quicksort.h"

// Macro swap
#define swap(x,y) {int t; t=x; x=y; y=t; g_num_trocas++;} 

const int CUTOFF1 = 34;
const int CUTOFF2 = 38;
const int CUTOFF3 = 35;

// ------------------------------------

void InsertionSort(int v[], int n)
{
   for(int i = 1; i < n; i++)
   {
      int e = v[i];
      int j = i-1;
      while(j >= 0 && v[j] > e)
      {
         g_num_comparacoes++;
         v[j+1] = v[j];
         j--;
      }
      v[j+1] = e;
      g_num_comparacoes += 1;
   }
}

// ------------------------------------

void Quicksort1(int v[], int n)
{
   if(n > 1)
   {
      // v[n-1] chosen as the pivot element
      int pivot = v[n-1];
      int i = -1, j = n-1;

      while(1)
      {
         // partition with one pivot
         g_num_comparacoes += 2;
         while(v[++i] < pivot) g_num_comparacoes++;
         while(v[--j] > pivot) g_num_comparacoes++;
         if(i >= j) break;
         swap(v[i], v[j]);
      }
      // puts pivot in the correct position
      swap(v[i], v[n-1]);

      // sort two parts
      Quicksort1(v, i);
      Quicksort1(v+i+1, n-i-1);
   }
}

// ------------------------------------

void Quicksort2(int v[], int n)
{
   int left = 0, right = n-1;
   if(right > left)
   {
      // v[left] and v[right] chosen as the pivot elements
      // [ < pivot1 | pivot1 <= && <= pivot2 | > pivot2 ]
      g_num_comparacoes++;
      if(v[left] > v[right]) swap(v[left], v[right]);

      int pivot1 = v[left], pivot2 = v[right];

      // from left+1 to l-1: elements < pivot1
      int l = left+1;

      // from l to k-1: pivot1 <= elements <= pivot2
      int k = l;

      // from g+1 to right-1: elements > pivot2
      int g = right-1;

      // from k to g: elements to be examined
      while(k <= g)
      {
         // partition with two pivots
         g_num_comparacoes++;
         if(v[k] < pivot1)
         {
            swap(v[k], v[l]);
            l++;
         }
         else if(v[k] >= pivot2)
         {
            g_num_comparacoes++;
            while(v[g] > pivot2 && k < g)
            {			
               g--;
               g_num_comparacoes++;
            }
            swap(v[k], v[g]);
            g--;
            g_num_comparacoes++;
            if(v[k] < pivot1)
            {
               swap(v[k], v[l]);
               l++;
            }
         }
         k++;
      }
      // update limits
      l--;
      g++;
      // puts pivots in the correct position 
      swap(v[left], v[l]);
      swap(v[right], v[g]);

      // sort three parts
      Quicksort2(v, l);
      Quicksort2(v+l+1, g-l-1);
      Quicksort2(v+g+1, n-g-1);
   }
}

// ------------------------------------

void Quicksort3(int v[], int n)
{
   int left = 0, right = n-1;
   if(right > left)
   {
      // sort elements chosen as pivots: v[left] < v[left+1] < v[right] 
      if(v[left] > v[left+1]) swap(v[left], v[left+1]);
      if(v[left] > v[right]) swap(v[left], v[right]);
      if(v[left+1] > v[right]) swap(v[left+1], v[right]);
      g_num_comparacoes += 3;

      // partitioning: 3 pivots
      int a = left+2, b = left+2;
      int c = right-1, d = right-1;
      int pivot1 = v[left];
      int pivot2 = v[left+1];
      int pivot3 = v[right];

      while(b <= c)
      {
         g_num_comparacoes++;
         while(v[b] < pivot2 && b <= c)
         {
            if(v[b] < pivot1)
            {
               swap(v[a], v[b]);
               a++;
            }
            b++;
            g_num_comparacoes += 2;
         }
         g_num_comparacoes++;
         while(v[c] > pivot2 && b <= c)
         {
            if(v[c] > pivot3)
            {
               swap(v[c], v[d]);
               d--;
            }
            c--;
            g_num_comparacoes += 2;
         }
         if(b <= c)
         {
            g_num_comparacoes++;
            if(v[b] > pivot3)
            {
               g_num_comparacoes++;
               if(v[c] < pivot1)
               {
                  swap(v[b], v[a]);
                  swap(v[a], v[c]);
                  a++;
               }
               else 
               {
                  swap(v[b], v[c]);
               }

               swap(v[c], v[d]);
               b++; c--; d--;
            }
            else
            {
               g_num_comparacoes++;
               if(v[c] < pivot1)
               {
                  swap(v[a], v[b]);
                  swap(v[a], v[c]);
                  a++;
               }
               else
               {
                  swap(v[b], v[c]);
               }
               b++; c--;
            }
         }
      }
      // update limits 
      a--; b--; c++; d++;
      // puts pivots in the correct position
      swap(v[left+1], v[a]);
      swap(v[a], v[b]);
      a--;
      swap(v[left], v[a]);
      swap(v[right], v[d]);

      // sort four parts
      Quicksort3(v, a);
      Quicksort3(v+a+1, b-a-1);
      Quicksort3(v+b+1, d-b-1);
      Quicksort3(v+d+1, n-d-1);
   }
}

// ------------------------------------

inline void choose_pivot(int *v, int n, int *pivots, int npivot)
{
   //considering there are enough elements in v
   for(int i = 0; i < 2*npivot+1; i++)
   {
      pivots[i] = rand()%(n-3) + 2;
      //check if pivots[i] is already chosen
      while(1)
      {
         bool was_chosen = false;
         for(int t = 0; t < i; ++t)
         {
            if(pivots[t] == pivots[i])
            {
               was_chosen = true;
               break;
            }
         }
         if(!was_chosen) break;
         pivots[i] = rand()%(n-3) + 2;
      }
      int index = pivots[i];
      int j = i-1;
      // puts element in the right position
      while(j >= 0 && v[pivots[j]] > v[index])
      {
         pivots[j+1] = pivots[j];
         j--;
         g_num_comparacoes++;
      }
      pivots[j+1] = index;
      g_num_comparacoes++;
   }
}

// ------------------------------------

void QuicksortH1(int v[], int n)
{
   if(n > 1)
   {
      // if vector has few elements, uses the insertion sort algorithm
      if(n < CUTOFF1)
      {
         InsertionSort(v, n);
         g_T_ins_sort = CUTOFF1;
         return;
      }

      int pivots[2*1+1];
      choose_pivot(v, n, pivots, 1);

      // from the pivots candidates, picks that in even position and swap 
      swap(v[n-1], v[pivots[1]]);
      int pivot = v[n-1];
      int i = -1, j = n-1;

      while(1)
      {
         // partition with one pivot
         g_num_comparacoes += 2;
         while(v[++i] < pivot) g_num_comparacoes++;
         while(v[--j] > pivot) g_num_comparacoes++;
         if(i >= j) break;
         swap(v[i], v[j]);
      }
      // puts pivot in the correct position
      swap(v[i], v[n-1]);

      // sort two parts
      QuicksortH1(v, i);
      QuicksortH1(v+i+1, n-i-1);
   }
}

// ------------------------------------

void QuicksortH2(int v[], int n)
{
   int left = 0, right = n-1;
   if(right > left)
   {
      //if vector has few elements, uses the insertion sort algorithm
      if(n < CUTOFF2)
      {
         InsertionSort(v, n);
         g_T_ins_sort = CUTOFF2;
         return;
      }

      int pivots[2*2+1];
      choose_pivot(v, n, pivots, 2);

      //from the pivots candidates, picks those in even position and swaps
      swap(v[left], v[pivots[1]]);
      swap(v[right], v[pivots[3]]);
      int pivot1 = v[left];
      int pivot2 = v[right];

      // from left+1 to l-1: elements < pivot1
      int l = left+1;

      // from l to k-1: pivot1 <= elements <= pivot2
      int k = l;

      // from g+1 to right-1: elements > pivot2
      int g = right-1;

      // from k to g: elements to be examined
      while(k <= g)
      {
         // partition with two pivots
         g_num_comparacoes++;
         if(v[k] < pivot1)
         {
            swap(v[k], v[l]);
            l++;
         }
         else if(v[k] >= pivot2)
         {
            g_num_comparacoes++;
            while(v[g] > pivot2 && k < g) 
            {
               g--;
               g_num_comparacoes++;
            }
            swap(v[k], v[g]);
            g--;
            g_num_comparacoes++;
            if(v[k] < pivot1)
            {
               swap(v[k], v[l]);
               l++;
            }
         }
         k++;
      }
      // update limits
      l--;
      g++;
      // puts pivots in the correct position 
      swap(v[left], v[l]);
      swap(v[right], v[g]);

      // sort three parts
      QuicksortH2(v, l);
      QuicksortH2(v+l+1, g-l-1);
      QuicksortH2(v+g+1, n-g-1);
   }
}

// ------------------------------------

void QuicksortH3(int v[], int n)
{
   int left = 0, right = n-1;
   if(right > left)
   {
      // if vector has few elements, uses the insertion sort algorithm
      if(n < CUTOFF3)
      {
         InsertionSort(v, n);
         g_T_ins_sort = CUTOFF3;
         return;
      }

      int pivots[2*3+1];
      choose_pivot(v, n, pivots, 3);

      // from the pivots candidates, picks those in even position and swaps
      swap(v[left], v[pivots[1]]);
      swap(v[left+1], v[pivots[3]]);
      swap(v[right], v[pivots[5]]);
      int pivot1 = v[left];
      int pivot2 = v[left+1];
      int pivot3 = v[right];

      // partitioning: 3 pivots
      int a = left+2, b = left+2;
      int c = right-1, d = right-1;

      while(b <= c)
      {
         g_num_comparacoes++;
         while(v[b] < pivot2 && b <= c)
         {
            if(v[b] < pivot1)
            {
               swap(v[a], v[b]);
               a++;
            }
            b++;
            g_num_comparacoes += 2;
         }
         g_num_comparacoes++;
         while(v[c] > pivot2 && b <= c)
         {
            if(v[c] > pivot3)
            {
               swap(v[c], v[d]);
               d--;
            }
            c--;
            g_num_comparacoes += 2;
         }
         if(b <= c)
         {
            g_num_comparacoes++;
            if(v[b] > pivot3)
            {
               g_num_comparacoes++;
               if(v[c] < pivot1)
               {
                  swap(v[b], v[a]);
                  swap(v[a], v[c]);
                  a++;
               }
               else 
               {
                  swap(v[b], v[c]);
               }

               swap(v[c], v[d]);
               b++; c--; d--;
            }
            else
            {
               g_num_comparacoes++;
               if(v[c] < pivot1)
               {
                  swap(v[a], v[b]);
                  swap(v[a], v[c]);
                  a++;
               }
               else
               {
                  swap(v[b], v[c]);
               }

               b++; c--;
            }
         }
      }
      // update limits 
      a--; b--; c++; d++;
      // puts pivots in the correct position
      swap(v[left+1], v[a]);
      swap(v[a], v[b]);
      a--;
      swap(v[left], v[a]);
      swap(v[right], v[d]);

      // sort four parts
      QuicksortH3(v, a);
      QuicksortH3(v+a+1, b-a-1);
      QuicksortH3(v+b+1, d-b-1);
      QuicksortH3(v+d+1, n-d-1);
   }
}

// ------------------------------------
// Debug
void print_vector(int *v, int n);
void copy_vector(int *v, int *q, int n);
bool is_sorted(int *v, int n);
// ------------------------------------

bool is_sorted(int *v, int n)
{
   for (int i = 1; i < n; ++i)
   {
      if(v[i] < v[i-1]) return false;
   }
   return true;
}

inline void print_vector(int *v, int n)
{
   for (int i = 0; i < n; ++i)
   {
      printf("%d ", v[i]);
   }
   printf("\n");
}

inline void copy_vector(int *v, int *q, int n)
{
   for (int i = 0; i < n; ++i)
   {
      q[i] = v[i];
   }
}
