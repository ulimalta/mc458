#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <algorithm>
#include <cassert>
#include <cstring>

//macro swap
#define swap(x,y) {int t; t=x; x=y; y=t;} 

//const int CUTOFF = 31;
const int CUTOFF = 31;

// ------------------------------------

void InsertionSort(int v[], int n)
{
   for(int i=1; i<n; i++)
   {
      int e = v[i];
      int j = i-1;
      while(j>=0 && v[j] > e)
      {
         v[j+1] = v[j];
         j--;
      }
      v[j+1] = e;
   }
}

// ------------------------------------

void Quicksort1(int v[], int n)
{
   if(n > 1)
   {
      // v[n-1] chosen as the pivot element
      int pivot = v[n-1];
      int i = -1, j = n-1;

      while(1)
      {
         // partition with one pivot
         while(v[++i] < pivot);
         while(v[--j] > pivot);
         if(i >= j) break;
         swap(v[i], v[j]);
      }
      // puts pivot in the correct position
      swap(v[i], v[n-1]);

      // sort two parts
      Quicksort1(v, i);
      Quicksort1(v+i+1, n-i-1);
   }
}

// ------------------------------------

void Quicksort2(int v[], int n)
{
   int left = 0, right = n-1;
   if(right > left)
   {
      // v[left] and v[right] chosen as the pivot elements
      // [ < pivot1 | pivot1 <= && <= pivot2 | > pivot2 ]
      if(v[left] > v[right]) swap(v[left], v[right]);
      int pivot1 = v[left], pivot2 = v[right];

      // from left+1 to l-1: elements < pivot1
      int l = left+1;

      // from l to k-1: pivot1 <= elements <= pivot2
      int k = l;

      // from g+1 to right-1: elements > pivot2
      int g = right-1;

      // from k to g: elements to be examined
      while(k<=g)
      {
         // partition with two pivots
         if(v[k] < pivot1)
         {
            swap(v[k], v[l]);
            l++;
         }
         else if(v[k] >= pivot2)
         {
            while(v[g] > pivot2 && k < g) g--;
            swap(v[k], v[g]);
            g--;
            if(v[k] < pivot1)
            {
               swap(v[k], v[l]);
               l++;
            }
         }
         k++;
      }
      // update limits
      l--;
      g++;
      // puts pivots in the correct position 
      swap(v[left], v[l]);
      swap(v[right], v[g]);

      // sort three parts
      Quicksort2(v, l);
      Quicksort2(v+l+1, g-l-1);
      Quicksort2(v+g+1, n-g-1);
   }
}

// ------------------------------------

void Quicksort3(int v[], int n)
{
   int left = 0, right = n-1;
   if(right > left)
   {
      // sort elements chosen as pivots: v[left] < v[left+1] < v[right] 
      if(v[left] > v[left+1]) swap(v[left], v[left+1]);
      if(v[left] > v[right]) swap(v[left], v[right]);
      if(v[left+1] > v[right]) swap(v[left+1], v[right]);

      // partitioning: 3 pivots
      int a = left+2, b = left+2;
      int c = right-1, d = right-1;
      int pivot1 = v[left];
      int pivot2 = v[left+1];
      int pivot3 = v[right];

      while(b <= c)
      {
         while(v[b] < pivot2 && b <= c)
         {
            if(v[b] < pivot1)
            {
               swap(v[a], v[b]);
               a++;
            }
            b++;
         }
         while(v[c] > pivot2 && b <= c)
         {
            if(v[c] > pivot3)
            {
               swap(v[c], v[d]);
               d--;
            }
            c--;
         }
         if(b <= c)
         {
            if(v[b] > pivot3)
            {
               if(v[c] < pivot1)
               {
                  swap(v[b], v[a]);
                  swap(v[a], v[c]);
                  a++;
               }
               else 
                  swap(v[b], v[c]);

               swap(v[c], v[d]);
               b++; c--; d--;
            }
            else
            {
               if(v[c] < pivot1)
               {
                  swap(v[a], v[b]);
                  swap(v[a], v[c]);
                  a++;
               }
               else
                  swap(v[b], v[c]);

               b++; c--;
            }
         }
      }
      // update limits 
      a--; b--; c++; d++;
      // puts pivots in the correct position
      swap(v[left+1], v[a]);
      swap(v[a], v[b]);
      a--;
      swap(v[left], v[a]);
      swap(v[right], v[d]);

      // sort four parts
      Quicksort3(v, a);
      Quicksort3(v+a+1, b-a-1);
      Quicksort3(v+b+1, d-b-1);
      Quicksort3(v+d+1, n-d-1);
   }
}

// ------------------------------------
inline void choose_pivot(int *v, int n, int *pivots, int npivot)
{
   //considering there are enough elements in v
   for (int i = 0; i < 2*npivot+1; ++i)
   {
      pivots[i] = rand()%(n-3) + 2;
      //check if pivots[i] is already chosen
      while(1)
      {
         bool was_chosen = false;
         for (int t = 0; t < i; ++t)
            if(pivots[t]==pivots[i])
            {
               was_chosen = true;
               break;
            }
         if(!was_chosen) break;
         pivots[i] = rand()%(n-3) + 2;
      }
      int index = pivots[i];
      int j = i-1;
      //puts element in the right position
      while(j>=0 && v[pivots[j]] > v[index])
      {
         pivots[j+1] = pivots[j];
         j--;
      }
      pivots[j+1] = index;
   }
}

// ------------------------------------
void QuicksortH1(int v[], int n)
{
   if(n > 1)
   {
      //if vector has few elements, uses the insertion sort algorithm
      if(n < CUTOFF)
      {
         InsertionSort(v, n);
         return;
      }
      
      int pivots[2*1+1];
      choose_pivot(v, n, pivots, 1);

      //from the pivots candidates, picks that in even position and swap 
      swap(v[n-1], v[pivots[1]]);
      int pivot = v[n-1];
      int i = -1, j = n-1;

      while(1)
      {
         // partition with one pivot
         while(v[++i] < pivot);
         while(v[--j] > pivot);
         if(i >= j) break;
         swap(v[i], v[j]);
      }
      // puts pivot in the correct position
      swap(v[i], v[n-1]);

      // sort two parts
      QuicksortH1(v, i);
      QuicksortH1(v+i+1, n-i-1);
   }
}

// ------------------------------------

void QuicksortH2(int v[], int n)
{
   int left = 0, right = n-1;
   if(right > left)
   {
      //if vector has few elements, uses the insertion sort algorithm
      if(n < CUTOFF)
      {
         InsertionSort(v, n);
         return;
      }
      
      int pivots[2*2+1];
      choose_pivot(v, n, pivots, 2);

      //from the pivots candidates, picks those in even position and swaps
      swap(v[left], v[pivots[1]]);
      swap(v[right], v[pivots[3]]);
      int pivot1 = v[left];
      int pivot2 = v[right];

      // from left+1 to l-1: elements < pivot1
      int l = left+1;

      // from l to k-1: pivot1 <= elements <= pivot2
      int k = l;

      // from g+1 to right-1: elements > pivot2
      int g = right-1;

      // from k to g: elements to be examined
      while(k<=g)
      {
         // partition with two pivots
         if(v[k] < pivot1)
         {
            swap(v[k], v[l]);
            l++;
         }
         else if(v[k] >= pivot2)
         {
            while(v[g] > pivot2 && k < g) g--;
            swap(v[k], v[g]);
            g--;
            if(v[k] < pivot1)
            {
               swap(v[k], v[l]);
               l++;
            }
         }
         k++;
      }
      // update limits
      l--;
      g++;
      // puts pivots in the correct position 
      swap(v[left], v[l]);
      swap(v[right], v[g]);

      // sort three parts
      QuicksortH2(v, l);
      QuicksortH2(v+l+1, g-l-1);
      QuicksortH2(v+g+1, n-g-1);
   }
}

// ------------------------------------
void QuicksortH3(int v[], int n)
{
   int left = 0, right = n-1;
   if(right > left)
   {
      //if vector has few elements, uses the insertion sort algorithm
      if(n < CUTOFF)
      {
         InsertionSort(v, n);
         return;
      }
      
      int pivots[2*3+1];
      choose_pivot(v, n, pivots, 3);

      //from the pivots candidates, picks those in even position and swaps
      swap(v[left], v[pivots[1]]);
      swap(v[left+1], v[pivots[3]]);
      swap(v[right], v[pivots[5]]);
      int pivot1 = v[left];
      int pivot2 = v[left+1];
      int pivot3 = v[right];

      // partitioning: 3 pivots
      int a = left+2, b = left+2;
      int c = right-1, d = right-1;

      while(b <= c)
      {
         while(v[b] < pivot2 && b <= c)
         {
            if(v[b] < pivot1)
            {
               swap(v[a], v[b]);
               a++;
            }
            b++;
         }
         while(v[c] > pivot2 && b <= c)
         {
            if(v[c] > pivot3)
            {
               swap(v[c], v[d]);
               d--;
            }
            c--;
         }
         if(b <= c)
         {
            if(v[b] > pivot3)
            {
               if(v[c] < pivot1)
               {
                  swap(v[b], v[a]);
                  swap(v[a], v[c]);
                  a++;
               }
               else 
                  swap(v[b], v[c]);

               swap(v[c], v[d]);
               b++; c--; d--;
            }
            else
            {
               if(v[c] < pivot1)
               {
                  swap(v[a], v[b]);
                  swap(v[a], v[c]);
                  a++;
               }
               else
                  swap(v[b], v[c]);

               b++; c--;
            }
         }
      }
      // update limits 
      a--; b--; c++; d++;
      // puts pivots in the correct position
      swap(v[left+1], v[a]);
      swap(v[a], v[b]);
      a--;
      swap(v[left], v[a]);
      swap(v[right], v[d]);

      // sort four parts
      QuicksortH3(v, a);
      QuicksortH3(v+a+1, b-a-1);
      QuicksortH3(v+b+1, d-b-1);
      QuicksortH3(v+d+1, n-d-1);
   }
}

// ------------------------------------
// debug
void print_vector(int *v, int n);
void copy_vector(int *v, int *q, int n);
bool is_sorted(int *v, int n);


int main(int argc, char *argv[])
{
   srand(time(NULL));

   int number_tests, n; // number of test cases
   scanf("%d %d", &number_tests, &n);

   // number of cycles for each algorithm
   double cycles_sort_q1 = 0;
   double cycles_sort_q2 = 0;
   double cycles_sort_q3 = 0;
   double cycles_sort_qH1 = 0;
   double cycles_sort_qH2 = 0;
   double cycles_sort_qH3 = 0;
   double cycles_sort_c = 0;

   for(int i=0; i<number_tests; ++i)
   {
      //int n;
      //scanf("%d", &n);
      int *v = (int*)malloc(n*sizeof(int));

      // read data
      for (int j = 0; j < n; ++j)
      {
         scanf("%d", &v[j]);
      }

      // debug
      // print_vector(v, n);

      int *q = (int*)malloc(n*sizeof(int));
      copy_vector(v, q, n);

      clock_t t;

      t = clock();
      Quicksort1(q, n);
      t = clock() - t;
      cycles_sort_q1 += t;

      //if not sorted, abort
      assert(is_sorted(q, n));

      //debug
      //printf("Quicksort1\n");
      //print_vector(q, n);

      copy_vector(v, q, n);

      t = clock();
      Quicksort2(q, n);
      t = clock() - t;
      cycles_sort_q2 += t;

      //if not sorted, abort
      assert(is_sorted(q, n));

      //debug
      // printf("Quicksort2\n");
      // print_vector(q, n);

      copy_vector(v, q, n);

      t = clock();
      Quicksort3(q, n);
      t = clock() - t;
      cycles_sort_q3 += t;

      //if not sorted, abort
      assert(is_sorted(q, n));

      // debug
      // printf("Quicksort3\n");
      // print_vector(q, n);

      copy_vector(v, q, n);

      t = clock();
      QuicksortH1(q, n);
      t = clock() - t;
      cycles_sort_qH1 += t;

      // debug
      //printf("QuicksortH1\n");
      //print_vector(q, n);
      
      //if not sorted, abort
      assert(is_sorted(q, n));

      copy_vector(v, q, n);

      t = clock();
      QuicksortH2(q, n);
      t = clock() - t;
      cycles_sort_qH2 += t;

      // debug
      //printf("QuicksortH2\n");
      //print_vector(q, n);
      
      //if not sorted, abort
      //std::cout << "H2" << std::endl;
      assert(is_sorted(q, n));

      copy_vector(v, q, n);

      t = clock();
      QuicksortH3(q, n);
      t = clock() - t;
      cycles_sort_qH3 += t;

      // debug
      //printf("QuicksortH3\n");
      //print_vector(q, n);
      
      //if not sorted, abort
      //std::cout << "H3" << std::endl;
      assert(is_sorted(q, n));


      copy_vector(v, q, n);

      //test performance for glibc sort algorithm
      t = clock();
      std::sort(q, q+n);
      t = clock() - t;
      cycles_sort_c += t;

      free(q);
      free(v);
   }

   //time for each algorithm
   double time_sort_q1 = (((double)cycles_sort_q1)/CLOCKS_PER_SEC)/number_tests;
   double time_sort_q2 = (((double)cycles_sort_q2)/CLOCKS_PER_SEC)/number_tests;
   double time_sort_q3 = (((double)cycles_sort_q3)/CLOCKS_PER_SEC)/number_tests;
   double time_sort_qH1 = (((double)cycles_sort_qH1)/CLOCKS_PER_SEC)/number_tests;
   double time_sort_qH2 = (((double)cycles_sort_qH2)/CLOCKS_PER_SEC)/number_tests;
   double time_sort_qH3 = (((double)cycles_sort_qH3)/CLOCKS_PER_SEC)/number_tests;
   double time_sort_c =  (((double)cycles_sort_c)/CLOCKS_PER_SEC)/number_tests;

   printf("Quicksort1   : %lf\n", time_sort_q1);
   printf("Quicksort2   : %lf\n", time_sort_q2);
   printf("Quicksort3   : %lf\n", time_sort_q3);
   printf("QuicksortH1  : %lf\n", time_sort_qH1);
   printf("QuicksortH2  : %lf\n", time_sort_qH2);
   printf("QuicksortH3  : %lf\n", time_sort_qH3);
   printf("Quicksort C++: %lf\n", time_sort_c);

   return 0;
}

bool is_sorted(int *v, int n)
{
   for (int i = 1; i < n; ++i)
   {
      if(v[i] < v[i-1]) return false;
   }
   return true;
}

inline void print_vector(int *v, int n)
{
   for (int i = 0; i < n; ++i)
   {
      printf("%d ", v[i]);
   }
   printf("\n");
}

inline void copy_vector(int *v, int *q, int n)
{
   for (int i = 0; i < n; ++i)
   {
      q[i] = v[i];
   }
}
