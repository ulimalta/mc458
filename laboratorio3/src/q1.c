﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define swap(x,y) { int t=x; x=y; y=t; }

/* Debug */
void print_vector(int *v, int n);
void copy_vector(int *v, int *q, int n);
void print_file(int v[], int n, const char *method, int flag);

/* ------------------------------------ */

void insertion_sort(int v[], int left, int right)
{
	int i;
	for(i = left + 1; i <= right; i++)
	{
		int e = v[i];
		int j = i - 1;
		while(j >= 0 && v[j] > e)
		{
			v[j+1] = v[j];
			j--;
		}
		v[j+1] = e;
	}
}

/* ------------------------------------ */

void quicksort1_rec(int v[], int left, int right)
{
	if(right > left)
	{
		/* v[right] chosen as the pivot element */
		int pivot = v[right];
		int i = left-1, j = right;

		while(1)
		{
			/* partition with one pivot */
			while(v[++i] < pivot);
			while(v[--j] > pivot);
			if(i >= j) break;
			swap(v[i], v[j]);
		}

		/* puts pivot in the correct position */
		swap(v[i], v[right]);

		/* sort two parts */
		quicksort1_rec(v, left, i-1);
		quicksort1_rec(v, i+1, right);
	}
}

void Quicksort1(int v[], int n)
{
	quicksort1_rec(v, 0, n-1);
}

/* ------------------------------------ */

void quicksort2_rec(int v[], int left, int right)
{
	if(right > left)
	{
		/* v[left] and v[right] chosen as the pivot elements */
		/* [ < pivot1 | pivot1 <= && <= pivot2 | > pivot2 ] */
		if(v[left] > v[right]) 
			swap(v[left], v[right]);

		int pivot1 = v[left], pivot2 = v[right];

		/* from left+1 to l-1: elements < pivot1 */
		int l = left + 1;

		/* from l to k-1: pivot1 <= elements <= pivot2   */
		int k = l;

		/* from g+1 to right-1: elements > pivot2 */
		int g = right - 1;

		/* from k to g: elements to be examined */
		while(k <= g)
		{
			/* partition with two pivots*/
			if(v[k] < pivot1)
			{
				swap(v[k], v[l]);
				l++;
			}
			else if(v[k] >= pivot2)
			{
				while(v[g] > pivot2 && k < g) g--;
				swap(v[k], v[g]);
				g--;
				if(v[k] < pivot1)
				{
					swap(v[k], v[l]);
					l++;
				}
			}
			k++;
		}

		/* update limits */
		l--;
		g++;

		/* puts pivots in the correct position */
		swap(v[left], v[l]);
		swap(v[right], v[g]);

		/* sort three parts */
		quicksort2_rec(v, left, l-1);
		quicksort2_rec(v, l+1, g-1);
		quicksort2_rec(v, g+1, right);
	}
}

void Quicksort2(int v[], int n)
{
	quicksort2_rec(v, 0, n-1);
}

/* ------------------------------------ */

void quicksort3_rec(int v[], int left, int right)
{
	if(right > left)
	{
		/* sort elements chosen as pivots: v[left] < v[left+1] < v[right] */
		if(v[left] > v[left+1]) 
			swap(v[left], v[left+1]);
		if(v[left] > v[right]) 
			swap(v[left], v[right]);
		if(v[left+1] > v[right]) 
			swap(v[left+1], v[right]);

		/* partitioning: 3 pivots */
		int a = left+2, b = left+2;
		int c = right-1, d = right-1;
		int pivot1 = v[left];
		int pivot2 = v[left+1];
		int pivot3 = v[right];

		while(b <= c)
		{
			while(v[b] < pivot2 && b <= c)
			{
				if(v[b] < pivot1)
				{
					swap(v[a], v[b]);
					a++;
				}
				b++;
			}
			while(v[c] > pivot2 && b <= c)
			{
				if(v[c] > pivot3)
				{
					swap(v[c], v[d]);
					d--;
				}
				c--;
			}
			if(b <= c)
			{
				if(v[b] > pivot3)
				{
					if(v[c] < pivot1)
					{
						swap(v[b], v[a]);
						swap(v[a], v[c]);
						a++;
					}
					else 
						swap(v[b], v[c]);

					swap(v[c], v[d]);
					b++; c--; d--;
				}
				else
				{
					if(v[c] < pivot1)
					{
						swap(v[a], v[b]);
						swap(v[a], v[c]);
						a++;
					}
					else
						swap(v[b], v[c]);

					b++; c--;
				}
			}
		}

		/* update limits */
		a--; b--; c++; d++;
		/* puts pivots in the correct position */
		swap(v[left+1], v[a]);
		swap(v[a], v[b]);
		a--;
		swap(v[left], v[a]);
		swap(v[right], v[d]);

		/* sort four parts */
		quicksort3_rec(v, left, a-1);
		quicksort3_rec(v, a+1, b-1);
		quicksort3_rec(v, b+1, d-1);
		quicksort3_rec(v, d+1, right);
	}
}

void Quicksort3(int v[], int n)
{
	quicksort3_rec(v, 0, n-1);
}

/* ------------------------------------ */

void choose_pivot(int v[], int left, int right, int npivots, int pivots[])
{
	if((right - left + 1) < (2*npivots+1) || left > right)
	{
		return;
	}
	int i, j, k = left;
	int index[right-left+1];
	int values[2*npivots+1], piv[2*npivots+1];
	srand(time(NULL));	
	for(i = 0; i < (right-left+1); i++)
		index[i] = i + left;
	for(i = 0; i < (2*npivots+1); i++)
	{
		j = rand() % (right - k + 1) + i;
		k++;
		swap(index[i], index[j]);
		j = i - 1;
		while(j >= 0 && values[j] > v[index[i]])
		{
			values[j+1] = values[j];
			piv[j+1] = piv[j];
			j--;
		}
		values[j+1] = v[index[i]];
		piv[j+1] = index[i];
	}
	j = 0;
	for(i = 1; i < (2*npivots+1); i += 2)
	{
		pivots[j] = piv[i];
		j++;
	}
}

/* ------------------------------------ */

void quicksortH1_rec(int v[], int left, int right)
{
	if(right > left)
	{
		if((right - left + 1) < 17)
		{
			insertion_sort(v, left, right);
			return;
		}

		int pivots[1];
		choose_pivot(v, left, right, 1, pivots);

		int pivot = v[pivots[0]];
		int i = left, j = right;

		do {
			while(v[i] < pivot) i++;
			while(pivot < v[j]) j--;
			if(i <= j)
			{
				swap(v[i], v[j]);
				i++; j--;
			}
		} while(i <= j);

		quicksortH1_rec(v, left, j);
		quicksortH1_rec(v, i, right);		
	}
}

void QuicksortH1(int v[], int n)
{
	quicksortH1_rec(v, 0, n-1);
}

/* ------------------------------------ */

void quicksortH2_rec(int v[], int left, int right)
{
	if(right > left)
	{
		if((right - left + 1) < 17)
		{
			insertion_sort(v, left, right);
			return;
		}
		
		int pivots[2];
		choose_pivot(v, left, right, 2, pivots);

		int pivot1 = v[pivots[0]], pivot2 = v[pivots[1]];

		swap(v[left], v[pivots[0]]);
		if(pivots[1] != left) 
		{
			swap(v[right], v[pivots[1]]); 
		}
		else
		{	
			swap(v[right], v[pivots[0]]);
		}

		/* from left+1 to l-1: elements < pivot1 */
		int l = left + 1;

		/* from l to k-1: pivot1 <= elements <= pivot2   */
		int k = l;

		/* from g+1 to right-1: elements > pivot2 */
		int g = right - 1;

		/* from k to g: elements to be examined */
		while(k <= g)
		{
			/* partition with two pivots*/
			if(v[k] < pivot1)
			{
				swap(v[k], v[l]);
				l++;
			}
			else if(v[k] >= pivot2)
			{
				while(v[g] > pivot2 && k < g) g--;
				swap(v[k], v[g]);
				g--;
				if(v[k] < pivot1)
				{
					swap(v[k], v[l]);
					l++;
				}
			}
			k++;
		}

		/* update limits */
		l--;
		g++;

		/* puts pivots in the correct position */
		swap(v[left], v[l]);
		swap(v[right], v[g]);

		/* sort three parts */
		quicksortH2_rec(v, left, l-1);
		quicksortH2_rec(v, l+1, g-1);
		quicksortH2_rec(v, g+1, right);
	}
}

void QuicksortH2(int v[], int n)
{
	quicksortH2_rec(v, 0, n-1);
}

/* ------------------------------------ */

void quicksortH3_rec(int v[], int left, int right)
{
	if(right > left)
	{
		if((right - left + 1) < 17)
		{
			insertion_sort(v, left, right);
			return;
		}
		
		int pivots[3];
		choose_pivot(v, left, right, 3, pivots);

		/* partitioning: 3 pivots */
		int a = left+2, b = left+2;
		int c = right-1, d = right-1;
		
		swap(v[left], v[pivots[0]]);
		if(pivots[1] == left)
		{
			swap(v[left+1], v[pivots[0]]);
		}
		else
		{
			swap(v[left+1], v[pivots[1]]);
		}
		
		if(pivots[2] == left && pivots[0] == left+1)
		{
			swap(v[right], v[pivots[1]]);
		}
		else if(pivots[2] == left)
		{
			swap(v[right], v[pivots[0]]);
		}
		else if(pivots[2] == left+1 && pivots[1] == left)
		{
			swap(v[right], v[pivots[0]]);
		}
		else if(pivots[2] == left+1)
		{
			swap(v[right], v[pivots[1]]);
		}
		else
		{
			swap(v[right], v[pivots[2]]);
		}		
		
		int pivot1 = v[left];
		int pivot2 = v[left+1];
		int pivot3 = v[right];

		while(b <= c)
		{
			while(v[b] < pivot2 && b <= c)
			{
				if(v[b] < pivot1)
				{
					swap(v[a], v[b]);
					a++;
				}
				b++;
			}
			while(v[c] > pivot2 && b <= c)
			{
				if(v[c] > pivot3)
				{
					swap(v[c], v[d]);
					d--;
				}
				c--;
			}
			if(b <= c)
			{
				if(v[b] > pivot3)
				{
					if(v[c] < pivot1)
					{
						swap(v[b], v[a]);
						swap(v[a], v[c]);
						a++;
					}
					else 
						swap(v[b], v[c]);

					swap(v[c], v[d]);
					b++; c--; d--;
				}
				else
				{
					if(v[c] < pivot1)
					{
						swap(v[a], v[b]);
						swap(v[a], v[c]);
						a++;
					}
					else
						swap(v[b], v[c]);

					b++; c--;
				}
			}
		}

		/* update limits */
		a--; b--; c++; d++;
		/* puts pivots in the correct position */
		swap(v[left+1], v[a]);
		swap(v[a], v[b]);
		a--;
		swap(v[left], v[a]);
		swap(v[right], v[d]);

		/* sort four parts */
		quicksortH3_rec(v, left, a-1);
		quicksortH3_rec(v, a+1, b-1);
		quicksortH3_rec(v, b+1, d-1);
		quicksortH3_rec(v, d+1, right);
	}
}

void QuicksortH3(int v[], int n)
{
	quicksortH3_rec(v, 0, n-1);
}

/* ------------------------------------ */

int main(int argc, char *argv[])
{
	int xt; /* number of test cases */
	int n, r;
	scanf("%d", &xt);
	r = xt;
	clock_t q1 = 0, q2 = 0, q3 = 0;
	clock_t qh1 = 0, qh2 = 0, qh3 = 0;	

	while(xt--)
	{
		int i;
		scanf("%d", &n);
		int *v = (int*)malloc(n*sizeof(int));
		for (i = 0; i < n; ++i)
		{
			scanf("%d", &v[i]);
		}

		int *q = (int*)malloc(n*sizeof(int));		
		clock_t t;

		copy_vector(v, q, n);

		t = clock();
		Quicksort1(q, n);
		t = clock() - t;
		q1 += t;
		/*print_vector(q, n);*/
		print_file(q, n, "Quicksort1", r-xt);
		
		copy_vector(v, q, n);

		t = clock();
		Quicksort2(q, n);
		t = clock() - t;
		q2 += t;
		/*print_vector(q, n);*/
		print_file(q, n, "Quicksort2", r-xt);

		copy_vector(v, q, n);

		t = clock();
		Quicksort3(q, n);
		t = clock() - t;
		q3 += t;
		/*print_vector(q, n);*/
		print_file(q, n, "Quicksort3", r-xt);

		copy_vector(v, q, n);

		t = clock();
		QuicksortH1(q, n);
		t = clock() - t;
		qh1 += t;
		/*print_vector(q, n);*/
		print_file(q, n, "QuicksortH1", r-xt);

		copy_vector(v, q, n);

		t = clock();
		QuicksortH2(q, n);
		t = clock() - t;
		qh2 += t;
		/*print_vector(q, n);*/
		print_file(q, n, "QuicksortH2", r-xt);

		t = clock();
		QuicksortH3(q, n);
		t = clock() - t;
		qh3 += t;
		/*print_vector(q, n);*/
		print_file(q, n, "QuicksortH3", r-xt);

		free(q);
		free(v);
	}
	
	double time_q1 = (((double)q1)/CLOCKS_PER_SEC)/r;
	double time_q2 = (((double)q2)/CLOCKS_PER_SEC)/r;
	double time_q3 = (((double)q3)/CLOCKS_PER_SEC)/r;
	double time_qh1 = (((double)qh1)/CLOCKS_PER_SEC)/r;
	double time_qh2 = (((double)qh2)/CLOCKS_PER_SEC)/r;
	double time_qh3 = (((double)qh3)/CLOCKS_PER_SEC)/r;

	printf("Time q1:  %.9lf\n", time_q1);
	printf("Time q2:  %.9lf\n", time_q2);
	printf("Time q3:  %.9lf\n", time_q3);
	printf("Time qh1: %.9lf\n", time_qh1);
	printf("Time qh2: %.9lf\n", time_qh2);
	printf("Time qh3: %.9lf\n", time_qh3);

	return 0;
}

void print_vector(int *v, int n)
{
	int i;
	for(i = 0; i < n; i++)
	{
		printf("%d ", v[i]);
	}
	printf("\n");
}

void copy_vector(int *v, int *q, int n)
{
	int i;
	for(i = 0; i < n; i++)
	{
		q[i] = v[i];
	}
}

void print_file(int v[], int n, const char *method, int flag)
{
	if(!v) 
		return;
	
	int i;
	char result_name[100];
	sprintf(result_name, "result_%s.txt", method);	
	
	FILE *result_file;
	if(flag != 1)
		result_file = fopen(result_name, "a");
	else
		result_file = fopen(result_name, "w");
	
	if(result_file != NULL)
	{
		for(i = 0; i < n; i++) 
			fprintf(result_file, "%d ", v[i]);
		fprintf(result_file, "\n");	
		fclose(result_file);
	}
}

