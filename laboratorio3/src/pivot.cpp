#include <bits/stdc++.h>

using namespace std;

#define D(x) cout << #x " is " << x << endl


inline void choose_pivot(int *v, int n, int *pivots, int npivot)
{
   //considering there are enough elements in v
   srand(time(NULL));
   bool was_chosen[n];
   memset(was_chosen, false, sizeof(was_chosen));
   for (int i = 0; i < 2*npivot+1; ++i)
   {
      pivots[i] = rand() % n;
      while(was_chosen[pivots[i]])
      {
         pivots[i] = rand() % n;
      }
      was_chosen[pivots[i]] = true;
      D(pivots[i]);
      int index = pivots[i];
      int j = i-1;
      //puts element in the right position
      while(j>=0 && v[pivots[j]] > v[index])
      {
         pivots[j+1] = pivots[j];
         j--;
      }
      pivots[j+1] = index;
   }
}

int q[1000];

void print_vector(int *v, int n)
{
	for(int i = 0; i < n; i++)
	{
		printf("(%d:%d) ", v[i], q[v[i]]);
	}
	printf("\n");
}

int main(void)
{
   clock_t t;
   
   int n = 100;

   srand(time(NULL));
   for (int i = 0; i < n; ++i)
   {
     q[i] = rand() % 100;
   }
	for(int i = 0; i < n; i++)
	{
		printf("%d ", q[i]);
	}
	printf("\n");
   
   int p[2*3+1];

   t = clock();
   choose_pivot(q, n, p, 3);
   t = clock()-t;
   printf("\n");
   print_vector(p, 7);

   printf("%lf\n", ((double)t)/CLOCKS_PER_SEC);
   return 0;
}
